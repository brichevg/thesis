from torch.utils.data import DataLoader
from JobsDataset import JobsDataset
from db_connector_2 import DbConnector2
import torch
import logging

from config import get_logger, Config
from padded_dataset import PaddedWrapperDataset, PaddedDatasetOptions, SchedulingDataset


class Loader:
    def __init__(self, connector, device, schedule_size):
        self.logger = get_logger('loader')
        self.config = Config()
        self.connector = connector
        padded_config_1 = PaddedDatasetOptions(max_size_x=(6, 4), max_size_y=(1, 6))
        self.training = SchedulingDataset()
        self.validation = SchedulingDataset()
        self.test = SchedulingDataset()
        # self.load_optimal_solutions_model_2_each(device, schedule_size)
        # self.load_optimal_solutions_model_2(device, schedule_size)
        self.load_all(device, schedule_size)
        # self.load_best_value_model(device, schedule_size)
        self.training = DataLoader(
            self.training, batch_size=self.config.hyperparams.batch_size, shuffle=True, num_workers=0
        )
        self.validation = DataLoader(
            self.validation, batch_size=self.config.hyperparams.batch_size, shuffle=False, num_workers=0
        )
        self.test = DataLoader(
            self.test, batch_size=self.config.hyperparams.batch_size, shuffle=False, num_workers=0
        )
        self.logger.info(f'Loaded {len(self.training.dataset)} samples for training.')
        self.logger.info(f'Loaded {len(self.validation.dataset)} samples for validation.')
        self.logger.info(f'Loaded {len(self.test.dataset)} samples for test.')

    def load_optimal_solutions_model_1(self, device, schedule_size):
        items = self.connector.read(schedule_size)
        offset = 0
        train_offset = round(len(items) / schedule_size * 0.70) * schedule_size
        val_offset = round(len(items) / schedule_size * 1.0) * schedule_size
        while offset < len(items) - schedule_size:
            if offset < train_offset:
                where_to_store = self.training
            elif offset < val_offset:
                where_to_store = self.validation
            else:
                where_to_store = self.test

            # vector of jobs in a schedule
            problem_samples = []
            # vector of tardiness in a schedule
            problem_labels = []
            tardy_amount = 0

            # retrieve probabilities
            p = torch.zeros(size=[schedule_size])
            opt_solution_amount = items[offset][1]
            for i in range(opt_solution_amount):
                for j in range(schedule_size):
                    if items[offset + j][7]:
                        p[j] += 1
            p = p / opt_solution_amount

            for i in range(schedule_size):
                problem_samples.append(
                    torch.Tensor(
                        [items[offset + i][3], items[offset + i][4], items[offset + i][5]]).to(
                        device))
                problem_labels.append(torch.Tensor([float(p[offset + i])]).to(device))

            where_to_store.push((torch.stack(problem_samples), torch.stack(problem_labels)))
            offset += schedule_size * opt_solution_amount

    def load_best_value_model(self, device, schedule_size):
        items = self.connector.read(schedule_size)
        # TODO
        offset = 0
        train_offset = round(len(items) / schedule_size * 0.60) * schedule_size
        val_offset = round(len(items) / schedule_size * 0.80) * schedule_size
        while offset < len(items) - schedule_size:
            if offset < train_offset:
                where_to_store = self.training
            elif offset < val_offset:
                where_to_store = self.validation
            else:
                where_to_store = self.test

            # vector of jobs in a schedule
            problem_samples = []
            # vector of tardiness in a schedule
            problem_labels = []
            tardy_amount = 0

            # input probabilities
            p = torch.zeros(size=[schedule_size])
            # output flags for all optimal solutions
            opt_solution_amount = items[offset][1]

            label = items[offset][8]

            for i in range(opt_solution_amount):
                for j in range(schedule_size):
                    if items[offset + i * schedule_size + j][7]:
                        p[j] += 1
            # label = label.unsqueeze(-1).unsqueeze(0)
            p = p / opt_solution_amount

            # label is the tardiness flags of all the optimal solutions

            for i in range(schedule_size):
                problem_samples.append(
                    torch.Tensor(
                        [items[offset + i][3], items[offset + i][4], items[offset + i][5], float(p[i])]).to(
                        device))
            where_to_store.push((torch.stack(problem_samples), torch.Tensor([label])))
            offset += schedule_size * opt_solution_amount

    def load_optimal_solutions_model_2_each(self, device, best_vals):
        for s in best_vals:
            self.load_optimal_solutions_model_2(device, s)

    def load_optimal_solutions_model_2(self, device, schedule_size):
        items = self.connector.read(schedule_size)
        # TODO
        offset = 0
        # only training
        train_offset = round(len(items) / schedule_size * 0.750) * schedule_size
        val_offset = round(len(items) / schedule_size * 1.00) * schedule_size
        while offset < len(items) - schedule_size:
            if offset < train_offset:
                where_to_store = self.training
            elif offset < val_offset:
                where_to_store = self.validation
            else:
                where_to_store = self.test

            # where_to_store = self.training

            # vector of jobs in a schedule
            problem_samples = []
            labels = []
            # vector of tardiness in a schedule
            # problem_labels = []
            # tardy_amount = 0
            #
            # # input probabilities
            p = torch.zeros(size=[schedule_size])
            # output flags for all optimal solutions
            opt_solution_amount = items[offset][1]

            label = torch.zeros(size=[schedule_size])

            for i in range(opt_solution_amount):
                for j in range(schedule_size):
                    if items[offset + i * schedule_size + j][7]:
                        p[j] += 1
            # label = label.unsqueeze(-1).unsqueeze(0)
            p = p / opt_solution_amount
            # for i in range(schedule_size):
            #     label[i] = items[offset + i][7]

            # label is the tardiness flags of all the optimal solutions

            value = 0
            for i in range(0, schedule_size):
                if items[offset + i][7]:
                    value = value + 1

            for i in range(schedule_size):
                problem_samples.append(
                    torch.Tensor(
                        [items[offset + i][4], items[offset + i][5], items[offset + i][6], p[i]]).to(
                        device))
                label[i] = items[offset + i][7]
            where_to_store.push((torch.stack(problem_samples), label.unsqueeze(0)))


            offset += schedule_size * opt_solution_amount

    def load(self, device, schedule_size):
        items = self.connector.read(schedule_size)
        offset = 0
        train_offset = round(len(items) / schedule_size * 0.60) * schedule_size
        val_offset = round(len(items) / schedule_size * 0.80) * schedule_size
        while offset < len(items) - schedule_size:
            if offset < train_offset:
                where_to_store = self.training
            elif offset < val_offset:
                where_to_store = self.validation
            else:
                where_to_store = self.test

            # vector of jobs in a schedule
            problem_samples = []
            # vector of tardiness in a schedule
            problem_labels = []
            for i in range(schedule_size):
                problem_samples.append(
                    torch.Tensor(
                        [items[offset + i][1], items[offset + i][2], items[offset + i][3]]).to(
                        device))
                problem_labels.append(torch.Tensor([float(items[offset + i][4])]).to(device))

            where_to_store.push((torch.stack(problem_samples), torch.stack(problem_labels)))
            offset += schedule_size

    def load_all(self, device, schedule_amounts):
        # TODO
        for i in schedule_amounts:
            self.logger.info(f'Loading schedules with {i} jobs.')
            self.load_optimal_solutions_model_2(device, i)
