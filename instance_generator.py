import os
import random
import time
import sys
from itertools import combinations_with_replacement
from db_connector import DbConnector

import numpy as np
import ilp

# connector = DbConnector2(
#     db_name=sys.argv[1],
#     db_user=sys.argv[2],
#     db_password=sys.argv[3],
#     db_host=sys.argv[4],
#     db_port="5432"
# )
connector = DbConnector(
    db_name="evgeniya",
    db_user="postgres",
    db_password="",
    db_host="localhost",
    db_port="5432"
)

def generate_by_sampling(n, p_max, samples_amount, alpha, beta):
    # generating processing times from [0;p_max]
    p = list(combinations_with_replacement(range(1, p_max + 1), n))
    random.shuffle(p)
    for p_i in p:
        r_i = [random.randint(1, round(beta * sum(p_i))) for _ in range(n)]
        s_i = [random.randint(1, round(alpha * sum(p_i))) for _ in range(n)]

        d_i = tuple(np.array(r_i) + np.array(p_i) + np.array(s_i))
        start = time.time()
        labels, best_val = ilp.solve_ilp(r_i, p_i, d_i)
        end = time.time()
        connector.save_problem(r_i, p_i, d_i, labels, best_val, n, end - start)
        samples_amount = samples_amount - 1
        if samples_amount == 0:
            break

for i in range(5,7):
    generate_by_sampling(i, 15, 10000, 0.4, 0.55)




