# inout args: db_user, db_password, db_host, schedule_size
import sys

import torch
import random
import numpy as np
from torch import nn
import torch.optim as optim
from db_connector import DbConnector
from db_connector_2 import DbConnector2
from losses import CountSolutionLoss, MinBCELoss

from loader_2 import Loader
from config import Config, ModelMode, get_logger
from net import Encoder, BahdanauDecoder

logger = get_logger('training_baseline')
config = Config()

if torch.cuda.is_available():
    # device = config.device
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

connector = DbConnector2(**config.db_opts._asdict())

# Setting up model
lr = config.hyperparams.lr
batch_size = config.hyperparams.batch_size
epochs = config.hyperparams.epochs
teacher_forcing_prob = config.sch_m_opts.teacher_forcing_prob

hidden_size = config.sch_m_opts.hidden_size
# hidden_size = 1024
input_size = config.sch_m_opts.input_size
output_size = config.sch_m_opts.output_size
encoder = Encoder(input_size, hidden_size, device).to(device)
decoder = BahdanauDecoder(output_size, hidden_size, output_size).to(device)

encoder_optimizer = optim.Adam(encoder.parameters(), lr=lr)
decoder_optimizer = optim.Adam(decoder.parameters(), lr=lr)
# criterion = config.sch_m_opts.loss()
count_criterion = CountSolutionLoss()
bce_criterion = MinBCELoss()
criterion = nn.MSELoss()

# TODO GENERATE MORE DATA TO SEE THE PERFORMANCE IN VALIDATION
# TODO CREATE GIT REPO
loader = Loader(connector, device, 5)


def run_epoch(loader, mode=ModelMode.train):
    mode_attr = {ModelMode.train: 'train', ModelMode.eval: 'eval'}.get(mode, 'train')
    encoder.__getattribute__(mode_attr)()
    decoder.__getattribute__(mode_attr)()
    offset = 0
    avg_loss = 0
    # tardy_count = {}
    instance_accuracy_train = []
    instance_accuracy_valid = []
    avg_loss, avg_acc = torch.empty(0), torch.empty(0)
    avg_bce_loss, avg_count_loss = torch.empty(0), torch.empty(0)
    for idx, (padded_x, mask_x, x_lengths, labels, mask_labels) in enumerate(loader):
        padded_x, mask_x, x_lengths, labels, mask_labels =\
            padded_x.to(device), mask_x.to(device), x_lengths.to(device), labels.to(device), mask_labels.to(device)
        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()
        h = encoder.init_hidden(padded_x.size(0))
        # jobs = torch.stack(jobs).unsqueeze(0).to(device)
        encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)

        # First "previous decoder output" will be the -1 token
        decoder_input = torch.empty((padded_x.size(0), 1, 1), device=device).fill_(-1)
        # First decoder hidden state will be last encoder hidden state
        decoder_hidden = h
        output = []
        for ii in range(labels.size(2)):
            decoder_output, decoder_hidden, attn_weights, value = decoder(decoder_input, decoder_hidden, encoder_outputs)
            # 1 - tardy, 0 - not tardy
            # predicted_value = 1. if decoder_output > 0.5 else 0.
            decoder_input = decoder_output.to(device)
            output.append(decoder_output.view(1, -1))
            # loss = loss + criterion(
            #     decoder_output.view(1, -1), labels[:, ii, :].view(1, -1), mask[:, ii, 0].view(1, -1)
            # )
            # loss = loss + decoder_output
            # accuracy_sum += abs(decoder_output.view(1, -1) * mask[:, ii, 0].view(1, -1) - labels[:, ii, :].view(1, -1))
            # print(f'Validation loss is {loss}.')
        prediction = torch.cat(output).view(padded_x.size(0), 1, -1, 1)
        target = labels.view(padded_x.size(0), -1, prediction.size(2), 1)
        mask_x = mask_x[:, :, 0].view(padded_x.size(0), 1, -1, 1)
        mask_target = mask_labels.view(padded_x.size(0), -1, prediction.size(2), 1)
        # loss, i = criterion(prediction, target, mask_x, mask_target)
        loss, i = criterion(value.sum(dim=2).view(1, 1, 1), target.sum(dim=2)), 0
        logger.debug(f'Prediction(opt_pred={value.sum(dim=2).item():.2f}, opt_target={target.sum(dim=2).item():.2f}).')
        count_loss = count_criterion(prediction, target, mask_x, mask_target)
        bce_loss, _ = bce_criterion(prediction, target, mask_x, mask_target)
        batch_indexing = torch.arange(0, padded_x.size(0)).long().view(padded_x.size(0), 1)
        # logger.debug(f'Prediction(sample={idx}, value={prediction.view(padded_x.size(0), -1).detach().numpy()})')
        # logger.debug(f'Target(sample={idx}, value={target[batch_indexing, i, :, :].long().view(padded_x.size(0), -1).detach().numpy()})')

        thr_preds = (prediction > 0.5).long().view(padded_x.size(0), -1)
        closest_targets = target[batch_indexing, i, :, :].long().view(padded_x.size(0), -1)
        acc = torch.all(thr_preds.eq(closest_targets), dim=1).double().mean()
        # print(loss.item())
        if mode == ModelMode.train:
            loss.backward()
            encoder_optimizer.step()
            decoder_optimizer.step()
        avg_loss = torch.cat((avg_loss, loss.view(1).detach())) if avg_loss.nelement() else loss.view(1).detach()
        avg_count_loss = torch.cat((avg_count_loss, count_loss.view(1).detach())) if avg_count_loss.nelement() else count_loss.view(1).detach()
        avg_bce_loss = torch.cat((avg_bce_loss, bce_loss.view(1).detach())) if avg_bce_loss.nelement() else bce_loss.view(1).detach()
        avg_acc = torch.cat((avg_acc, acc.view(1).detach())) if avg_acc.nelement() else acc.view(1).detach()
        # instance_accuracy_train.append((accuracy_sum / x_lengths.view(1, -1)).sum() / batch_size)
    return avg_loss.mean().item(), avg_acc.mean().item(), avg_bce_loss.mean().item(), avg_count_loss.mean().item()


for epoch in range(epochs):
    loss, acc, bce, count = run_epoch(loader=loader.training, mode=ModelMode.train)
    logger.info(f'Train(epoch={epoch}, loss={loss:.4f}, mA={acc:.4f}, bce={bce:.4f}, count={count:.4f}).')
    loss, acc, bce, count = run_epoch(loader=loader.validation, mode=ModelMode.eval)
    # logger.info(f'Validation(epoch={epoch}, loss={loss:.4f}, mA={acc:.4f}, bce={bce:.4f}, count={count:.4f}).')
    # loss, acc, bce, count = run_epoch(loader=loader.test, mode=ModelMode.eval)
    # logger.info(f'Test(epoch={epoch}, loss={loss:.4f}, mA={acc:.4f}, bce={bce:.4f}, count={count:.4f}).')
