# inout args: db_user, db_password, db_host, schedule_size
import sys
import torch
import random
import numpy as np
from torch import nn
import torch.optim as optim
import logging
from db_connector import DbConnector
from db_connector_2 import DbConnector2

from loader import Loader
# from config import get_logger
from losses import SchedulingLoss
from net import Encoder, BahdanauDecoder

# logger = get_logger('main')

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

# logger.info('Starting DbConnector.')
# connector = DbConnector(db_name=sys.argv[1],
#                         db_user=sys.argv[2],
#                         db_password=sys.argv[3],
#                         db_host=sys.argv[4],
#                         db_port="5432")
connector = DbConnector(
    db_name="evgeniya",
    db_user="postgres",
    db_password="",
    db_host="localhost",
    db_port="5432"
)
# logger.info('DbConnector started successfully.')

hidden_size = 256
input_size = 3
output_size = 1

encoder = Encoder(input_size, hidden_size, device).to(device)
decoder = BahdanauDecoder(output_size, hidden_size, output_size).to(device)
lr = 0.0005
batch_size = 64
encoder_optimizer = optim.Adam(encoder.parameters(), lr=lr)
decoder_optimizer = optim.Adam(decoder.parameters(), lr=lr)

EPOCHS = 200
teacher_forcing_prob = 0.5
criterion = SchedulingLoss(nn.L1Loss())
# criterion = SchedulingLoss(MinBCELoss())

old_val_loss = np.inf

# test_loss = 0
loader = Loader(connector, device, [7, 8, 9, 10, 11, 12])
# loader.load_all(device, [7, 8, 9, 10, 11, 12])
# loader = Loader(connector, device, [5,6])
for epoch in range(1, EPOCHS + 1):
    encoder.train()
    decoder.train()

    offset = 0
    avg_loss = 0
    # tardy_count = {}
    instance_accuracy_train = []
    instance_accuracy_valid = []
    for i, (padded_x, mask, x_lengths, labels, _) in enumerate(loader.training):
        # learn by a single problem
        loss = 0
        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()
        h = encoder.init_hidden(padded_x.size(0))
        # jobs = torch.stack(jobs).unsqueeze(0).to(device)
        encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)

        # First "previous decoder output" will be the -1 token
        decoder_input = torch.empty((padded_x.size(0), 1, 1), device=device).fill_(-1)
        # First decoder hidden state will be last encoder hidden state
        decoder_hidden = h
        output = []

        # some of the sensences (schedules) are with the teacher forcing
        teacher_forcing = True if random.random() < teacher_forcing_prob else False

        # sum of differences between label and job
        accuracy_sum = 0
        output = []
        for ii in range(labels.size(2)):
            decoder_output, decoder_hidden, attn_weights = decoder(decoder_input, decoder_hidden, encoder_outputs)
            # 1 - tardy, 0 - not tardy
            # predicted_value = 1. if decoder_output > 0.5 else 0.
            if teacher_forcing:
                decoder_input = labels[:, :, ii].to(device)
            else:
                decoder_input = decoder_output.to(device)
            loss = loss + criterion(
                decoder_output.view(1, -1), labels[:, :, ii].view(1, -1), mask[:, ii, 0].view(1, -1)
            )
            # loss = loss + decoder_output
            accuracy_sum += abs(decoder_output.view(1, -1)*mask[:, ii, 0].view(1, -1) - labels[:, :, ii].view(1, -1)).cpu()
                # print(f'Validation loss is {loss}.')
        if epoch > 1:
            loss.backward()
            encoder_optimizer.step()
            decoder_optimizer.step()
        # avg_loss += loss.item() / len(train_items)
        instance_accuracy_train.append((accuracy_sum / x_lengths.view(1, -1)).sum() / padded_x.size(0))

    # validation
    encoder.eval()
    decoder.eval()

    val_avg_loss = 0
    offset = 0
    for padded_x, mask, x_lengths, labels, _ in loader.validation:
        # learn by a single problem
        loss = 0
        h = encoder.init_hidden(padded_x.size(0))
        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()
        # jobs = torch.stack(jobs).unsqueeze(0).to(device)
        encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)

        # First "previous decoder output" will be the -1 token
        decoder_input = torch.empty((padded_x.size(0), 1), device=device).fill_(-1)
        # First decoder hidden state will be last encoder hidden state
        decoder_hidden = h
        output = []

        # some of the sensences (schedules) are with the teacher forcing
        teacher_forcing = True if random.random() < teacher_forcing_prob else False
        criterium_val = 0
        accuracy_sum = 0
        for ii in range(labels.size(2)):
            decoder_output, decoder_hidden, attn_weights = decoder(decoder_input, decoder_hidden, encoder_outputs)
            decoder_input = decoder_output.to(device)
            loss = loss + criterion(
                decoder_output.view(1, -1), labels[:, :, ii].view(1, -1), mask[:, ii, 0].view(1, -1)
            )
            accuracy_sum += abs(decoder_output.view(1, -1)*mask[:, ii, 0].view(1, -1) - labels[:, :, ii].view(1, -1)).cpu()

        # print(f'Validation loss is {loss}.')
        instance_accuracy_valid.append((accuracy_sum / x_lengths.view(1, -1)).sum() / padded_x.size(0))
        # val_avg_loss += loss.item() / len(val_items)

    if old_val_loss > val_avg_loss:
        old_val_loss = val_avg_loss
        torch.save(encoder, 'encoder.pt')
        torch.save(decoder, 'decoder.pt')
    acc_train = 1. - (sum(instance_accuracy_train) / len(instance_accuracy_train)).item()
    print(f'Train(epoch={epoch}, loss={avg_loss:.4f}, mA={(acc_train):.4f}).')
    acc_validation = 1. - (sum(instance_accuracy_valid) / len(instance_accuracy_valid)).item()
    print(f'Validation(epoch={epoch}, loss={avg_loss:.4f}, mA={(acc_validation):.4f}).')

# test
# encoder = torch.load('encoder.pt')
# decoder = torch.load('decoder.pt')
# # encoder = Encoder(input_size, hidden_size, device).to(device)
# # decoder = BahdanauDecoder(output_size, hidden_size, output_size).to(device)
# decoder.eval()
# encoder.eval()
# test_avg_loss = 0
# offset = 0
# correct = 0
#
# correct_criterium = 0
# while offset != len(test_items):
#     # learn by a single problem
#     jobs = []
#     labels = []
#     test_loss = 0
#     # one "sentence" of 4 jobs = 1 problem
#     for j in range(offset, offset + 4):
#         jobs.append(torch.Tensor([test_items[j][1], test_items[j][2], test_items[j][3]]))
#         labels.append(torch.Tensor([test_items[j][4]]))
#
#     # for each problem
#     h = encoder.init_hidden()
#     encoder_outputs, h = encoder(torch.stack(jobs).unsqueeze(0), h)
#
#     # First "previous decoder output" will be the -1 token
#     decoder_input = torch.tensor([-1.], device=device)
#     # First decoder hidden state will be last encoder hidden state
#     decoder_hidden = h
#     output = []
#
#     predicted_values = []
#     count_predicted_correctly = 0
#
#     criterium_real = 0
#     criterium_predicted = 0
#
#     for ii in range(len(labels)):
#         decoder_output, decoder_hidden, attn_weights = decoder(decoder_input, decoder_hidden, encoder_outputs)
#         # 1 - tardy, 0 - not tardy
#         predicted_value = 0. if decoder_output < 0.5 else 1.
#         predicted_values.append(torch.Tensor([predicted_value]))
#         decoder_input = torch.tensor([predicted_value], device=device)
#         test_loss = test_loss + bce(decoder_output, labels[ii])
#         criterium_real = criterium_real + labels[ii]
#         criterium_predicted = criterium_predicted + predicted_value
#
#     if criterium_predicted == criterium_real:
#         correct_criterium = correct_criterium + 1
#
#     offset = offset + 4
#     test_avg_loss += test_loss.item() / len(test_items)
#
# print("Test loss: ", test_avg_loss, ", correctly predicted: ", correct_criterium)
