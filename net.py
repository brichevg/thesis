import torch
from torch import nn
import numpy as np

from padded_modules import PaddedLSTM


class Encoder(nn.Module):
    def __init__(self, input_size, hidden_size, device, n_layers=1, drop_prob=0):
        super(Encoder, self).__init__()
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.device = device
        self.lstm = PaddedLSTM(input_size, hidden_size, n_layers, dropout=drop_prob, batch_first=True)

    def forward(self, inputs, x_lengths, hidden):
        output, hidden = self.lstm(inputs, x_lengths, hidden)
        return output, hidden

    def init_hidden(self, batch_size=1):
        return (torch.zeros(self.n_layers, batch_size, self.hidden_size, device=self.device),
                torch.zeros(self.n_layers, batch_size, self.hidden_size, device=self.device))

class LuongDecoder(nn.Module):
    def __init__(self, hidden_size, output_size, attention, n_layers=1, drop_prob=0.1):
        super(LuongDecoder, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.n_layers = n_layers
        self.drop_prob = drop_prob

        # The Attention Mechanism is defined in a separate class
        self.attention = attention

        self.embedding = nn.Embedding(self.output_size, self.hidden_size)
        self.dropout = nn.Dropout(self.drop_prob)
        self.lstm = nn.LSTM(self.hidden_size, self.hidden_size)
        self.classifier = nn.Linear(self.hidden_size * 2, self.output_size)



class BahdanauDecoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, n_layers=1, drop_prob=0.1):
        super(BahdanauDecoder, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.n_layers = n_layers
        self.drop_prob = drop_prob

        # because we pass last predicted value + concat vector??
        self.lstm_input_size = input_size + hidden_size

        # self.embedding = nn.Embedding(self.output_size, self.hidden_size)

        self.fc_hidden = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.fc_encoder = nn.Linear(self.hidden_size, self.hidden_size, bias=False)
        self.weight = nn.Parameter(torch.rand(1, hidden_size), requires_grad=True)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.dropout = nn.Dropout(self.drop_prob)
        # input to LSTM is teh CV + prev. output?
        self.lstm = nn.LSTM(input_size + hidden_size, self.hidden_size, batch_first=True)
        self.classifier = nn.Linear(2 * self.hidden_size, self.output_size)
        self.regressor = nn.Sequential(
            # nn.Linear(2 * self.hidden_size, self.hidden_size),
            # nn.ReLU(),
            nn.Linear(self.hidden_size, self.hidden_size // 2),
            nn.ReLU(),
            nn.Linear(self.hidden_size // 2, self.output_size)
        )

    def forward(self, inputs, hidden, encoder_outputs, mask_x):
        # encoder_outputs = encoder_outputs.squeeze()
        # Embed input words
        # inputs = inputs.view(1, 1, -1)
        inputs = self.dropout(inputs.view(1, 1, -1))

        # feed forward net/ why hidden[0]?
        masked_encoder_outputs = encoder_outputs * mask_x
        # x = torch.tanh(self.fc_hidden(hidden[0].permute(1, 0, 2)) + self.fc_encoder(encoder_outputs))
        x = torch.tanh(self.fc_hidden(hidden[0].permute(1, 0, 2)) + self.fc_encoder(masked_encoder_outputs))

        masked_x = x * mask_x
        # alignment_scores = x.bmm(self.weight.unsqueeze(2).repeat(x.size(0), 1, 1))
        alignment_scores = masked_x.bmm(self.weight.unsqueeze(2).repeat(x.size(0), 1, 1))

        # Softmaxing alignment scores to get Attention weights
        attn_weights = torch.softmax(alignment_scores.view(x.size(0), 1, -1), dim=0)
        masked_attn_weights = attn_weights * mask_x.permute(0, 2, 1)
        # Multiplying the Attention weights with encoder outputs to get the context vector
        context_vector = torch.bmm(masked_attn_weights, encoder_outputs)

        # Concatenating context vector with embedded input word
        output = torch.cat((inputs.view(x.size(0), 1, 1), context_vector), dim=2)
        # Passing the concatenated vector as input to the LSTM cell
        output, hidden = self.lstm(output, hidden)
        # Passing the LSTM output through a Linear layer acting as a classifier
        # output = nn.log_softmax(self.classifier(output[0]), dim=1)

        # output = torch.cat((output, hidden[0].permute(1, 0, 2)), dim=2)
        output_pred = torch.relu(self.regressor(hidden[0].permute(1, 0, 2)))
        output_reg = self.regressor(output)
        return output_pred, hidden, attn_weights, output_reg
