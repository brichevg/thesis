import torch
import torch.nn as nn
import torch.utils.data
from torch import Tensor
from typing import Tuple, Optional


class PaddedLSTM(nn.Module):
    """
    Use with the same interface as a regular LSTM layer,
    except for the forward call, where
    the input x is expected to be padded,
    and the original lengths for each element in the batch are expected:
    >> lstm(x, x_lengths)
    """
    def __init__(self, *args, **kwargs):
        super(PaddedLSTM, self).__init__()
        self.lstm = nn.LSTM(*args, **kwargs)

    def forward(self, x: Tensor, x_lengths: Tensor, hidden: Optional[Tuple[Tensor, Tensor]])\
            -> Tuple[Tensor, Tuple[Tensor, Tensor]]:
        # x_lengths = Tensor([5])
        T = x.size(1)
        x = nn.utils.rnn.pack_padded_sequence(x, x_lengths, batch_first=True, enforce_sorted=False)
        x, self.hidden = self.lstm(x, hidden)
        x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, total_length=T, batch_first=True)
        return x, self.hidden


class PaddedLoss(nn.Module):
    """
    Use as criterion = PaddedLoss(nn.BCELoss())
    then loss = criterion(predictions, targets, mask)
    """
    def __init__(self, criterion: nn.Module):
        super(PaddedLoss, self).__init__()
        self.criterion = criterion

    def forward(self, input: Tensor, target: Tensor, mask: Tensor) -> Tensor:
        return self.criterion(input * mask, target)
