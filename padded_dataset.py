from typing import List, NamedTuple, Tuple
from torch.utils.data import Dataset
import torch.nn.functional as F
import torch


class PaddedDatasetOptions(NamedTuple):
    # src: str = './data'
    # set max size as a param
    #
    max_size_x: Tuple[int, int]
    max_size_y: Tuple[int, int]
    padding_fractions_x: Tuple[float] = (1., 0., 0., 0.)
    padding_fractions_y: Tuple[float] = (1., 0., 1., 0.)


class SchedulingDataset(Dataset):
    def __init__(self):
        self.samples = []

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx]

    def push(self, element):
        self.samples.append(element)


class PaddedWrapperDataset(SchedulingDataset):
    def __init__(self, opts: PaddedDatasetOptions):
        super(PaddedWrapperDataset, self).__init__()
        self.max_size_x = opts.max_size_x
        self.max_size_y = opts.max_size_y
        self.fractions_x = torch.Tensor(opts.padding_fractions_x)
        self.fractions_y = torch.Tensor(opts.padding_fractions_y)

    def __len__(self) -> int:
        return len(self.samples)

    # def __getitem__(self, idx: int):
    #     x, y = super(PaddedWrapperDataset, self).__getitem__(idx)
    #     # T x W
    #     T, W = x.size()
    #     # S x T
    #     S, T = y.size()
    #     pad_x = torch.IntTensor(list(reversed(self.max_size_x))) - torch.IntTensor(list(reversed([T, W])))
    #     pad_y = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([S, T])))
    #     pad_y1 = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([self.max_size_y[0], T])))
    #     pad_y2 = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([S, self.max_size_y[1]])))
    #     fracs_x = torch.IntTensor(list(reversed(self.fractions_x)))
    #     fracs_y = torch.IntTensor(list(reversed(self.fractions_y)))
    #     padded_x = F.pad(
    #         input=x, mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_x, repeats=2) * fracs_x).int().tolist()
    #     )
    #     mask_x = F.pad(
    #         input=torch.ones_like(x), mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_x, repeats=2) * fracs_x).int().tolist()
    #     )
    #     padded_y = F.pad(
    #         input=y, mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_y, repeats=2) * fracs_y).int().tolist()
    #     )
    #     mask_y = F.pad(
    #         input=torch.ones_like(y), mode='constant', value=1,
    #         pad=(torch.repeat_interleave(pad_y1, repeats=2) * fracs_y).int().tolist()
    #     )
    #     mask_y = F.pad(
    #         input=mask_y, mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_y2, repeats=2) * fracs_y).int().tolist()
    #     )
    #     # returns padded tensor, mask, and length of sequence
    #     return padded_x, mask_x, torch.IntTensor([T]).view(1, 1).long(), padded_y, mask_y

    def __getitem__(self, idx: int):
        x, y = super(PaddedWrapperDataset, self).__getitem__(idx)
        # T x W
        T, W = x.size()
        # S x T
        S, T = y.size()
        pad_x = torch.IntTensor(list(reversed(self.max_size_x))) - torch.IntTensor(list(reversed([T, W])))
        pad_y = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([S, T])))
        pad_y1 = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([self.max_size_y[0], T])))
        pad_y2 = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([S, self.max_size_y[1]])))
        fracs_x = torch.IntTensor(list(reversed(self.fractions_x)))
        fracs_y = torch.IntTensor(list(reversed(self.fractions_y)))
        padded_x = F.pad(
            input=x, mode='constant', value=0,
            pad=(torch.repeat_interleave(pad_x, repeats=2) * fracs_x).int().tolist()
        )
        mask_x = F.pad(
            input=torch.ones_like(x), mode='constant', value=0,
            pad=(torch.repeat_interleave(pad_x, repeats=2) * fracs_x).int().tolist()
        )

        padded_y = F.pad(
            input=y, mode='constant', value=0,
            pad=(torch.repeat_interleave(pad_y, repeats=2) * fracs_y).int().tolist()
        )
        mask_y = F.pad(
            input=torch.ones_like(y), mode='constant', value=0,
            pad=(torch.repeat_interleave(pad_y1, repeats=2) * fracs_y).int().tolist()
        )
        mask_y = F.pad(
            input=mask_y, mode='constant', value=0,
            pad=(torch.repeat_interleave(pad_y2, repeats=2) * fracs_y).int().tolist()
        )
        # returns padded tensor, mask, and length of sequence
        return padded_x, mask_x, mask_x[:, 0].sum().view(1, 1).long(), padded_y, mask_y

    # return padded_x, mask_x, torch.IntTensor([T]).view(1, 1).long(), y

    # def __getitem__(self, idx: int):
    #     x, y = super(PaddedWrapperDataset, self).__getitem__(idx)
    #     # T x W
    #     T, W = x.size()
    #     # S x T
    #     T = y.size()
    #     pad_x = torch.IntTensor(list(reversed(self.max_size_x))) - torch.IntTensor(list(reversed([T, W])))
    #     pad_y = torch.IntTensor([6]) - torch.IntTensor(list(T))
    #     # pad_y1 = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([self.max_size_y[0], T])))
    #     # pad_y2 = torch.IntTensor(list(reversed(self.max_size_y))) - torch.IntTensor(list(reversed([S, self.max_size_y[1]])))
    #     fracs_x = torch.IntTensor(list(reversed(self.fractions_x)))
    #     fracs_y = torch.IntTensor(list(reversed(self.fractions_y)))
    #     padded_x = F.pad(
    #         input=x, mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_x, repeats=2) * fracs_x).int().tolist()
    #     )
    #     mask_x = F.pad(
    #         input=torch.ones_like(x), mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_x, repeats=2) * fracs_x).int().tolist()
    #     )
    #
    #     padded_y = F.pad(
    #         input=y, mode='constant', value=0,
    #         pad=(torch.repeat_interleave(pad_y, repeats=2) * fracs_y).int().tolist()
    #     )
    #     mask_y = F.pad(
    #         input=torch.ones_like(y), mode='constant', value=1,
    #         pad=(torch.repeat_interleave(pad_y1, repeats=2) * fracs_y).int().tolist()
    #     )
    #     # mask_y = F.pad(
    #     #     input=mask_y, mode='constant', value=0,
    #     #     pad=(torch.repeat_interleave(pad_y2, repeats=2) * fracs_y).int().tolist()
    #     # )
    #     # returns padded tensor, mask, and length of sequence
    #     return padded_x, mask_x, torch.IntTensor([T]).view(1, 1).long(), padded_y, mask_y

