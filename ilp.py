import time

import gurobipy as g
import numpy as np


def solve_ilp(r, p, d):
    global result
    result = []

    global callback_count
    callback_count = 0

    global due_dates
    # d = (5, 5, 20, 30)
    due_dates = d

    global release_dates
    # r = (1, 1, 11, 20)
    release_dates = r

    global proc_times
    # p = (3, 3, 4, 6)
    proc_times = p

    model = g.Model()
    # model.Params.lazyConstraints = 1

    global lazy_switches
    lazy_switches = []

    for j in range(0, len(r)):
        lazy_switches.append(model.addVar(lb=0, ub=1, vtype=g.GRB.BINARY, name='ls_1' + str(j)))
    model.addConstr(sum(lazy_switches) <= len(lazy_switches) - 1)

    # maximum time unit
    T = max(r) + sum(p)

    # x_jt = 1 if job j starts at time t
    global x
    x = np.empty((len(r), T), dtype=object)
    for j in range(0, len(r)):
        for t in range(0, T):
            x[j][t] = model.addVar(lb=0, ub=1, vtype=g.GRB.BINARY, name='x_' + str(j) + '_' + str(t))
    obj = 0
    for j in range(len(r)):
        j_time_units = 0
        for t in range(T):

            # for 1st constraint
            if t < r[j]:
                model.addConstr(x[j][t] == 0, str(j) + " starts after release date")

            # for the objective function
            if t + p[j] > d[j]:
                obj = obj + x[j][t]

            # for 3rd constraint
            j_time_units = j_time_units + x[j][t]

        model.addConstr(j_time_units == 1, "job " + str(j) + " is scheduled")

    # for 2nd constraint
    for t in range(T):

        # the area where just a single job can be started
        area = 0

        for j in range(len(r)):
            for t_ in range(t - p[j] + 1, t + 1):
                area = area + x[j][t_]
        model.addConstr(area <= 1, "no jobs overlap at time " + str(t))

    model.setObjective(obj, sense=g.GRB.MINIMIZE)
    best_val = 1000
    while True:
        model.optimize()
        if model.status == 3:
            return result, best_val

        if best_val >= model.ObjVal:
            best_val = model.ObjVal
        else:
            return result, best_val

        current_result = []
        for j in range(len(x)):
            for t in range(len(x[0])):
                if x[j][t].x == 1:
                    if t + proc_times[j] > due_dates[j]:
                        current_result.append(1)
                    else:
                        current_result.append(0)
        result.append(current_result)
        cb(model, x)


def cb(model, x):
    M = 1000000

    for j in range(0, len(x)):
        for t in range(0, len(x[0])):

            # job j starts at time t
            if x[j][t].x == 1:

                # job is tardy
                if t + proc_times[j] > due_dates[j]:
                    constr1 = 0
                    for i in range(due_dates[j] - proc_times[j] + 1, len(x[0])):
                        constr1 = constr1 + x[j][i]
                    model.addConstr(constr1 - lazy_switches[j] * M <= 0)

                # job j is ok
                else:
                    constr2 = 0
                    for i in range(due_dates[j] - proc_times[j] + 1, len(x[0])):
                        constr2 = constr2 + x[j][i]
                    model.addConstr(constr2 + lazy_switches[j] * M >= 1)