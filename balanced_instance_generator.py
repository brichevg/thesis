import os
import random
import time
import sys
from itertools import combinations_with_replacement
from db_connector import DbConnector

import numpy as np
import ilp

connector = DbConnector(
    db_name=sys.argv[1],
    db_user=sys.argv[2],
    db_password=sys.argv[3],
    db_host=sys.argv[4],
    db_port="5432"
)
# connector = DbConnector(
#     db_name="evgeniya",
#     db_user="postgres",
#     db_password="",
#     db_host="localhost",
#     db_port="5432"
# )


def generate_instance(p_max, schedule_size, alpha, beta):
    p = [random.randint(1, p_max) for _ in range(schedule_size)]
    r = [random.randint(1, round(alpha * sum(p))) for _ in range(schedule_size)]
    s = [random.randint(1, round(beta * sum(p))) for _ in range(schedule_size)]
    d = tuple(np.array(r) + np.array(p) + np.array(s))

    start = time.time()
    labels, best_val = ilp.solve_ilp(r, p, d)
    end = time.time()
    connector.save_problem(r, p, d, labels, best_val, schedule_size, end - start)


P_MAX = 10
for schedule_size in [50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]:
    for alpha in [0.25, 0.5, 0.75]:
        for beta in [0.1, 0.25, 0.75]:
                generate_instance(P_MAX, schedule_size, alpha, beta)
