from torch.utils.data import DataLoader
import torch

from config import get_logger, Config
from min_distance_model.scheduling_dataset import SchedulingDataset


class Loader:
    def __init__(self, connector, device, schedule_size, batch_size, only_test=False):
        self.logger = get_logger(f'loader_sch_{schedule_size}')
        self.connector = connector
        self.training = SchedulingDataset()
        self.validation = SchedulingDataset()
        self.test = SchedulingDataset()
        self.instance_amount = 0
        self.load(device, schedule_size, only_test)

        if not only_test:
            self.training = DataLoader(
                self.training, batch_size=batch_size, shuffle=True, num_workers=0
            )
            self.validation = DataLoader(
                self.validation, batch_size=batch_size, shuffle=False, num_workers=0
            )
        self.test = DataLoader(
            self.test, batch_size=batch_size, shuffle=False, num_workers=0
        )
        # self.logger.info(f'Loaded {len(self.training.dataset)} samples for training.')
        # self.logger.info(f'Loaded {len(self.validation.dataset)} samples for validation.')
        # self.logger.info(f'Loaded {len(self.test.dataset)} samples for test.')

    def load(self, device, schedule_size, only_test = False):
        items = []
        problems_amount = 0
        if device == torch.device('cpu'):
            items_1 = self.connector.read_from_local(schedule_size=schedule_size, best_val=1, max_amount=1232)
            items.extend(items_1)
            problems_amount = problems_amount + len(items_1)
            items_2 = self.connector.read_from_local(schedule_size=schedule_size, best_val=2, max_amount=1283)
            items.extend(items_2)
            problems_amount = problems_amount + len(items_2)
            items_3 = self.connector.read_from_local(schedule_size=schedule_size, best_val=3, max_amount=1076)
            items.extend(items_3)
            problems_amount = problems_amount + len(items_3)
            items_4 = self.connector.read_from_local(schedule_size=schedule_size, best_val=4, max_amount=1000)
            items.extend(items_4)
            problems_amount = problems_amount + len(items_4)
        else:
            items_1 = self.connector.read(schedule_size=schedule_size, best_val=1, max_amount=1232)
            items.extend(items_1)
            problems_amount = problems_amount + len(items_1)
            items_2 = self.connector.read(schedule_size=schedule_size, best_val=2, max_amount=1283)
            items.extend(items_2)
            problems_amount = problems_amount + len(items_2)
            items_3 = self.connector.read(schedule_size=schedule_size, best_val=3, max_amount=1076)
            items.extend(items_3)
            problems_amount = problems_amount + len(items_3)
            items_4 = self.connector.read(schedule_size=schedule_size, best_val=4, max_amount=1000)
            items.extend(items_4)
            problems_amount = problems_amount + len(items_4)
        # TODO

        self.instance_amount = self.instance_amount + len(items)
        offset = 0
        train_offset = 0.6
        val_offset = 0.8
        problems_counter = 0
        while True:
            problems_counter = problems_counter + 1
            if problems_counter / problems_amount < train_offset:
                where_to_store = self.training
            elif problems_counter / problems_amount < val_offset:
                where_to_store = self.validation
            else:
                where_to_store = self.test

            solution_amount = items[offset][1]
            samples = []
            label = torch.zeros(size=[100, schedule_size])
            p = torch.zeros(size=[schedule_size])
            # output flags for all optimal solutions

            for i in range(solution_amount):
                for j in range(schedule_size):
                    try:
                        label[i, j] = items[offset + i * schedule_size + j][7]
                    except:
                        print(3)
                    if items[offset + i * schedule_size + j][7]:
                        p[j] += 1
            label[solution_amount:100, :] = label[solution_amount - 1].repeat(100 - solution_amount).view(-1,
                                                                                                          schedule_size)
            p = p / solution_amount

            for i in range(schedule_size):
                samples.append(
                    torch.Tensor([items[offset + i][4], items[offset + i][5], items[offset + i][6], p[i]]).to(device))
            where_to_store.push((torch.stack(samples), label))
            offset = offset + schedule_size * solution_amount
            if offset > len(items) - 1:
                break

    def load_all(self, device, schedule_amounts):
        for i in schedule_amounts:
            self.logger.info(f'Loading schedules with {i} jobs.')
            self.load(device, i)
