# inout args: db_user, db_password, db_host, schedule_size
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import torch
import numpy as np
from torch import nn
import torch.optim as optim
from min_distance_model.db_connector import DbConnector
from min_distance_model.losses import NoMaskMinBCELoss
from min_distance_model.scheduling import list_scheduling_alg, early_tardy_sort_scheduling, order_by_e
from min_distance_model.loader import Loader
from min_distance_model.config import Config, ModelMode, get_logger
from min_distance_model.net import Encoder, LuongDecoder, Attention
from min_distance_model.evaluation_ilp import eval

logger = get_logger('training_baseline')

if torch.cuda.is_available():
    device = torch.device('cuda')
    config = Config('cuda_config.ini')
else:
    device = torch.device('cpu')
    config = Config('cpu_config.ini')

connector = DbConnector(**config.db_opts._asdict())
lr = config.hyperparams[0].lr
batch_size = config.hyperparams[0].batch_size
epochs = config.hyperparams[0].epochs
teacher_forcing_prob = config.model_opts.teacher_forcing_prob

hidden_size = config.model_opts.hidden_size
input_size = config.model_opts.input_size
output_size = config.model_opts.output_size

# models
attention = Attention(hidden_size, method="concat")

encoder = Encoder(input_size, hidden_size, device)
encoder.to(device)

decoder = LuongDecoder(hidden_size, output_size, attention=attention)
decoder.to(device)

# by sizes
accuracy = []
regressor = nn.Sequential(
    nn.Linear(hidden_size, 1),
    nn.ReLU()
)
regressor.to(device)

encoder_optimizer = optim.Adam(encoder.parameters(), lr=lr)
decoder_optimizer = optim.Adam(decoder.parameters(), lr=lr)
regressor_optimizer = optim.Adam(regressor.parameters(), lr=lr)

criterion = NoMaskMinBCELoss()
criterion2 = nn.L1Loss()
schedule_sizes = [int(schedule_size) for schedule_size in config.sch_size[0].split(',')]

# key = size, value = loader instance
loaders = []
loader_stat = []
total_amount = 0


def run_epoch(loader):
    gap = []
    instance_gap = []
    losses, tardy_accs, value_accs = [], [], []
    correct_sched, correct_list_sched, correct_ordered_by_e, total = 0, 0, 0, 0
    for i, (x, label) in enumerate(loader):
        instance_gap = []
        x, label = x.to(device), label.to(device)
        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()
        h = encoder.init_hidden(x.size(0))
        encoder_outputs, h = encoder(x, h)
        # First "previous decoder output" will be the -1 token
        decoder_input = torch.empty((1, 1, x.size(0)), device=device).fill_(0.5)
        # First decoder hidden state will be last encoder hidden state
        decoder_hidden = h
        output = []
        # amount of tardy jobs
        pred_value = regressor(encoder_outputs.sum(dim=1))
        for ii in range(label.size(2)):
            decoder_output, decoder_hidden, attn_weights = decoder(decoder_input.view(-1, 1, 1), decoder_hidden,
                                                                   encoder_outputs)
            decoder_input = decoder_output.unsqueeze(0).to(device)
            output.append(decoder_output)

        tardy_loss, argmin = criterion(torch.stack(output, dim=1), label.permute(0, 2, 1).unsqueeze(-1))

        # cause in all optimal solutions same number of tardy jobs
        opt_val = label[:, 0, :].squeeze(1).sum(dim=1).unsqueeze(1)
        value_acc = pred_value.round().eq(opt_val.round()).double().detach()
        value_accs.append(value_acc)

        tardy_pred = torch.zeros((label.size(0), label.size(2)))
        # prediction based on value - picking y2 highest probablities
        for i in range(len(pred_value)):
            val = min(int(pred_value[i].round().item()), label.size(2) - 1)
            if val != 0:
                output_i = torch.topk(torch.stack(output, dim=1).view(x.size(0), -1)[i].squeeze().detach(), val)
                tardy_pred[i, output_i[1]] = 1
        dest_solution = torch.gather(label, 1, argmin.view(-1, 1).unsqueeze(2).repeat(1, 1, label.size(2)))
        tardy_acc = tardy_pred.eq(dest_solution.squeeze().cpu()).all(dim=1).double().detach()
        tardy_accs.append(tardy_acc)
        gap.extend(torch.abs(tardy_pred.view(-1).cpu() - dest_solution.squeeze().view(-1).cpu()).tolist())
        instance_gap.append(sum(torch.abs(tardy_pred.view(-1).cpu() - dest_solution.squeeze().view(-1).cpu()).tolist()))/label.size(2).cpu()

        loss = criterion2(pred_value, opt_val) + tardy_loss
        losses.append(loss.item())

        # evaluation list scheduling
        jobs = prepare_for_eval(x, torch.stack(output, dim=1))
        for i in range(x.size()[0]):
            ordered = early_tardy_sort_scheduling(jobs[i].tolist(), x.size()[1] - int(pred_value[i].round().item()))
            ordered = np.stack(ordered)
            ordered = ordered.astype(int)
            value = eval(ordered[:, 0], ordered[:, 1], ordered[:, 2])
            total += 1
            correct_sched += 1 if opt_val[i].round().item() == value else 0

            # list_sched
            ordered_list_sched = list_scheduling_alg(jobs[i].tolist(), x.size()[1] - int(pred_value[i].round().item()))
            ordered_list_sched = np.stack(ordered_list_sched)
            ordered_list_sched = ordered_list_sched.astype(int)
            value = eval(ordered_list_sched[:, 0], ordered_list_sched[:, 1], ordered_list_sched[:, 2])
            correct_list_sched += 1 if opt_val[i].round().item() == value else 0

            # ordered by e
            ordered_by_e = order_by_e(jobs[i].tolist())
            ordered_by_e = np.stack(ordered_by_e)
            ordered_by_e = ordered_by_e.astype(int)
            value = eval(ordered_by_e[:, 0], ordered_by_e[:, 1], ordered_by_e[:, 2])
            correct_ordered_by_e += 1 if opt_val[i].round().item() == value else 0

    return correct_ordered_by_e / total if total else 0, correct_sched / total if total else 0, correct_list_sched/total if total else 0, torch.cat(tardy_accs, dim=0).mean(dim=0).item(), torch.cat(value_accs,
                                                                                                             dim=0).mean(
        dim=0).item(), torch.cat(tardy_accs, dim=0).std(dim=0).item(), sum(gap)/len(gap), sum(instance_gap)/len(instance_gap)


def prepare_for_eval(x, output):
    probabilities = output.view(x.size()[0], -1)
    x[:, :, 3] = torch.ones_like(probabilities) - probabilities
    return x


encoder = torch.load('encoder.pt')
decoder = torch.load('decoder.pt')
regressor = torch.load('regressor.pt')
encoder.eval()
decoder.eval()
regressor.eval()

sizes = [6, 7, 8, 9, 10, 15, 20, 25, 30, 35]
correct_sched_all, correct_list_sched_all, ordered_by_e_all, std_all, ta  = [], [], [], [], []
for size in sizes:
    loader = Loader(connector, device, size, batch_size)
    ordered_by_e,  correct_sched, correct_list_sched, tardy_acc, val_acc, std, gap, inst_gap = run_epoch(loader.test)
    logger.info(f'Test(vA={val_acc:.4f}, tA={tardy_acc:.4f},ordered_by_e={ordered_by_e:.4f},inst_gap={inst_gap:.4f},gap={gap:.4f}, correct_sched={correct_sched:.4f}), correct_list_sched={correct_list_sched:.4f}).')
    correct_sched_all.append(correct_sched)
    correct_list_sched_all.append(correct_list_sched)
    ordered_by_e_all.append(ordered_by_e)
    ta.append(tardy_acc)
    std_all.append(std)


fig, (ax0) = plt.subplots(nrows=1, sharex=True)
ax0.errorbar(sizes, correct_sched_all)
ax0.errorbar(sizes, correct_list_sched_all)
ax0.errorbar(sizes, ordered_by_e_all)
ax0.set_title('accuracy of prediction by heuristics')
blue_patch = mpatches.Patch(color='blue', label='my heuristics')
orange_patch = mpatches.Patch(color='orange', label='list scheduling')
green_patch = mpatches.Patch(color='green', label='ordering by e')
plt.legend(handles=[blue_patch, orange_patch, green_patch])
plt.savefig('heuristics.png')
fig2, (ax1) = plt.subplots(nrows=1, sharex=True)
ax1.errorbar(sizes, ta, yerr=std_all, fmt='-o')
ax1.set_title("model accuracy")
plt.show()
plt.savefig('trady_accuracy.png')
