import gurobipy as g
import numpy as np


# r,p,d are already given in the order calculated by nn
def eval(r, p, d):
    model = g.Model()
    model.setParam('OutputFlag', 0)

    # maximum time unit
    T = max(r) + sum(p)

    # x_jt = 1 if job j starts at time t
    x = np.empty((len(r), T), dtype=object)

    for j in range(0, len(r)):
        for t in range(0, T):
            x[j][t] = model.addVar(lb=0, ub=1, vtype=g.GRB.BINARY, name='x_' + str(j) + '_' + str(t))
    obj = 0
    for j in range(len(r)):
        j_time_units = 0
        for t in range(T):

            # for 1st constraint
            # if t < r[j]:
            #     model.addConstr(x[j][t] == 0, str(j) + " starts after release date")

            # for the objective function
            if t + p[j] > d[j]:
                obj = obj + x[j][t]

            # for 3rd constraint
            j_time_units = j_time_units + x[j][t]

        model.addConstr(j_time_units == 1, "job " + str(j) + " is scheduled")


    # for 2nd constraint
    # for t in range(T):
    #
    #     # the area where just a single job can be started
    #     area = 0
    #
    #     for j in range(len(r)):
    #         for t_ in range(t - p[j] + 1, t + 1):
    #             area = area + x[j][t_]
    #     model.addConstr(area <= 1, "no jobs overlap at time " + str(t))

    # particular solution based on the order given

    # x[0][r[0]] = 1
    model.addConstr(x[0][r[0]] == 1, "job 0 is scheduled according to the NN output")
    offset = r[0] + p[0]

    for i in range(1, len(r)):
        model.addConstr(x[i][offset] == 1, "job " + str(i) + " is scheduled according to the NN output")
        if r[i] > offset:
            offset = r[i] + p[i]
        else:
            offset = offset + p[i]
        # x[i][offset] = 1


    model.setObjective(obj, sense=g.GRB.MINIMIZE)
    model.optimize()

    return int(model.ObjVal)
