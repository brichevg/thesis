import psycopg2


class DbConnector:

    def __init__(self, db_name, db_user, db_password, db_host, db_port):
        connection = None
        try:
            connection = psycopg2.connect(
                database=db_name,
                user=db_user,
                password=db_password,
                host=db_host,
                port=db_port,
            )
            print("Connection to PostgreSQL DB successful")
        except psycopg2.OperationalError as e:
            print("The error occurred")
        self.connection = connection

    def create_connection(self, db_name, db_user, db_password, db_host, db_port):
        connection = None
        try:
            connection = psycopg2.connect(
                database=db_name,
                user=db_user,
                password=db_password,
                host=db_host,
                port=db_port,
            )
            print("Connection to PostgreSQL DB successful")
        except psycopg2.OperationalError as e:
            print("The error occurred")
        self.connection = connection

    def retrieve_labels(self, t_matrix):
        labels = []
        counter = {}
        # vector of assignments for the problem
        for solution in t_matrix:
            for j in range(len(solution)):
                if j not in counter:
                    counter[j] = 0
                if solution[j]:
                    counter[j] += 1
        for j in range(len(counter)):
            labels.append(counter[j] / len(t_matrix))
        return labels

    def save_problem(self, r, p, d, t_matrix, best_val, jobs_amount, p_time):
        cursor = self.connection.cursor()

        # problem
        insert_problem = """ INSERT INTO scheduling_data_2.problems (best_val, opt_solution_amount, jobs_amount, p_time) VALUES (""" + str(
            str(best_val) + "," + str(len(t_matrix)) + "," + str(jobs_amount) + ", " + str(
                p_time)) + """) RETURNING id"""
        cursor.execute(insert_problem, ())
        self.connection.commit()
        problem_id = cursor.fetchone()[0]
        # labels = self.retrieve_labels(t_matrix)

        for i in range(len(t_matrix)):
            # optimal solution
            insert_opt_solution = """ INSERT INTO scheduling_data_2.optimal_solutions (problem_id) VALUES (""" + str(
                problem_id) + """) RETURNING id"""

            cursor.execute(insert_opt_solution, ())
            opt_solution_id = cursor.fetchone()[0]

            # jobs
            jobs = []

            for k in range(len(t_matrix[i])):
                jobs.append((opt_solution_id, r[k], p[k], d[k].item(), bool(t_matrix[i][k])))

            job_records = ", ".join(["%s"] * len(jobs))

            insert_jobs = (
                f"INSERT INTO scheduling_data_2.jobs (optimal_solution_id, r, p, d, tardy) VALUES {job_records}"
            )
            cursor.execute(insert_jobs, jobs)
        self.connection.commit()

    def read(self, schedule_size, best_val, max_amount):
        select_problems = """select p.id, 
                                    p.opt_solution_amount, 
                                    p.jobs_amount, 
                                    o.id, 
                                    j.r, 
                                    j.p, 
                                    j.d, 
                                    j.tardy, 
                                    p.best_val
                from problems p 
                inner join 
                optimal_solutions o 
                on 
                p.id = o.problem_id 
                inner join 
                jobs j 
                on 
                o.id = j.optimal_solution_id 
                where p.jobs_amount = """ \
                          + str(schedule_size) \
                          + """ and p.best_val = """ \
                          + str(best_val) \
                          + """ order by p.id,o.id limit """ \
                          + str(max_amount)

        cursor = self.connection.cursor()
        try:
            cursor.execute(select_problems)
            result = cursor.fetchall()
            return result
        except psycopg2.OperationalError as e:
            print("The error occurred")


    def read_from_local(self, schedule_size, best_val, max_amount):
        select_problems = """select p.id, 
                                    p.opt_solution_amount,  
                                    p.jobs_amount, 
                                    o.id, 
                                    j.r, 
                                    j.p, 
                                    j.d, 
                                    j.tardy, 
                                    p.best_val
                from scheduling_data_2.problems p 
                inner join scheduling_data_2.optimal_solutions o 
                on p.id = o.problem_id 
                inner join
                scheduling_data_2.jobs j 
                on o.id = j.optimal_solution_id 
                where p.jobs_amount = """ \
                          + str(schedule_size) \
                          + """ and p.best_val = """ \
                          + str(best_val) \
                          + """ order by p.id,o.id limit """ \
                          + str(max_amount)

        cursor = self.connection.cursor()
        try:
            cursor.execute(select_problems)
            result = cursor.fetchall()
            return result
        except psycopg2.OperationalError as e:
            print("The error occurred")

    def get_problem_amount(self, schedule_size):
        select_count = """select count(*) from scheduling_data_2.problems where jobs_amount = """ + str(schedule_size)
        cursor = self.connection.cursor()
        try:
            cursor.execute(select_count)
            result = cursor.fetchall()
            return result
        except psycopg2.OperationalError as e:
            print("The error occurred")