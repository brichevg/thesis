import logging
from enum import IntEnum
from typing import NamedTuple, Any
from configparser import ConfigParser
from torch.optim import Adam

from losses import MultipleSchedulingLoss


def get_logger(name, level=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


class ModelMode(IntEnum):
    train = 0
    valid = 1
    test = 2


class DbOpts(NamedTuple):
    db_name: str = 'evgeniya'
    db_user: str = 'postgres'
    db_password: str = ''
    db_host: str = 'localhost'
    db_port: int = 5432


class Hyperparameters(NamedTuple):
    lr: float = 0.01 * 32. / 256
    batch_size: int = 64
    epochs: int = 150


class SchedulingModelOpts(NamedTuple):
    # hidden_size: int = 2000
    hidden_size: int = 200
    input_size: int = 4
    output_size: int = 1
    teacher_forcing_prob: float = 0.5
    optimizer: Any = Adam
    loss: Any = MultipleSchedulingLoss


class Config:
    def __init__(self, path):
        self._from = ConfigFile(path)
        self.db_opts = self._from.db_opts
        self.hyperparams = self._from.hyperparams
        self.sch_size = self._from.sch_size
        self.model_opts = self._from.model_opts


class ConfigFile():
    def __init__(self, path):
        config = ConfigParser()
        config.read(path)
        # self.device = TorchDevice(config.get('GENERAL', 'device', fallback=None))
        self.db_opts = DbOpts(
            db_name=config.get('DB', 'db_name', fallback=None),
            db_user=config.get('DB', 'db_user', fallback=None),
            db_password=config.get('DB', 'db_password', fallback=None),
            db_host=config.get('DB', 'db_host', fallback=None),
            db_port=config.getint('DB', 'db_port', fallback=None)
        )
        self.hyperparams = Hyperparameters(
            lr=config.getfloat('HYPERPARAMETERS', 'lr', fallback=None),
            batch_size=config.getint('HYPERPARAMETERS', 'batch_size', fallback=None),
            epochs=config.getint('HYPERPARAMETERS', 'epochs', fallback=None)
        ),
        self.model_opts = SchedulingModelOpts(
            hidden_size=config.getint('MODELOPTS', 'hidden_size', fallback=None),
            input_size=config.getint('MODELOPTS', 'input_size', fallback=None),
            output_size=config.getint('MODELOPTS', 'output_size', fallback=None),
            teacher_forcing_prob=config.getfloat('MODELOPTS', 'teacher_forcing_prob', fallback=None),
            optimizer=config.get('MODELOPTS', 'optimizer', fallback=None),
            loss=config.get('MODELOPTS', 'loss', fallback=None),
        )
        self.sch_size = config.get('LOADER', 'schedule_size', fallback=None),
