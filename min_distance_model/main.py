# inout args: db_user, db_password, db_host, schedule_size

import random
import torch
import numpy as np
from torch import nn
import torch.optim as optim
from min_distance_model.db_connector import DbConnector
from min_distance_model.losses import NoMaskMinBCELoss
from min_distance_model.scheduling import list_scheduling_alg, early_tardy_sort_scheduling
from min_distance_model.loader import Loader
from min_distance_model.config import Config, ModelMode, get_logger
from min_distance_model.net import Encoder, LuongDecoder, Attention
from min_distance_model.evaluation_ilp import eval

logger = get_logger('training_baseline')

if torch.cuda.is_available():
    device = torch.device('cuda')
    config = Config('cuda_config.ini')
else:
    device = torch.device('cpu')
    config = Config('cpu_config.ini')

connector = DbConnector(**config.db_opts._asdict())
lr = config.hyperparams[0].lr
batch_size = config.hyperparams[0].batch_size
epochs = config.hyperparams[0].epochs
teacher_forcing_prob = config.model_opts.teacher_forcing_prob

hidden_size = config.model_opts.hidden_size
input_size = config.model_opts.input_size
output_size = config.model_opts.output_size

# models
attention = Attention(hidden_size, method="concat")

encoder = Encoder(input_size, hidden_size, device)
encoder.to(device)

decoder = LuongDecoder(hidden_size, output_size, attention=attention)
decoder.to(device)

regressor = nn.Sequential(
    nn.Linear(hidden_size, 1),
    nn.ReLU()
)
regressor.to(device)

encoder_optimizer = optim.Adam(encoder.parameters(), lr=lr)
decoder_optimizer = optim.Adam(decoder.parameters(), lr=lr)
regressor_optimizer = optim.Adam(regressor.parameters(), lr=lr)

criterion = NoMaskMinBCELoss()
criterion2 = nn.L1Loss()
schedule_sizes = [int(schedule_size) for schedule_size in config.sch_size[0].split(',')]

# key = size, value = loader instance
loaders = []
loader_stat = []
total_amount = 0

iterators_train = []
iterators_validation = []
iterators_test = []


def prepare_for_eval(x, output):
    probabilities = output.view(x.size()[0], -1)
    x[:, :, 3] = torch.ones_like(probabilities) - probabilities
    return x


def renew_iterators():
    global iterators_train
    iterators_train = []

    global iterators_validation
    iterators_validation = []

    global iterators_test
    iterators_test = []

    for i in range(len(schedule_sizes)):
        loader = loaders[i]
        iterators_train.append(iter(loader.training))
        iterators_validation.append(iter(loader.validation))
        iterators_test.append(iter(loader.test))


for i in range(len(schedule_sizes)):
    loader = Loader(connector, device, schedule_sizes[i], batch_size)
    loaders.append(loader)
    total_amount = total_amount + loader.instance_amount

renew_iterators()

# loader completion
loader_comp = {}
# create statistics
for i in schedule_sizes:
    loader_stat.append(i / total_amount)


def get_next(mode, loader_i):
    if mode == ModelMode.valid:
        iterator = iterators_validation[loader_i]
    elif mode == ModelMode.train:
        iterator = iterators_train[loader_i]
    else:
        iterator = iterators_test[loader_i]
    return next(iterator)


def run_epoch(mode=ModelMode.train):
    renew_iterators()
    availability = np.ones(len(schedule_sizes))
    losses, tardy_accs, value_accs = [], [], []
    correct, total = 0, 0
    while True:
        loader_i = -1
        while loader_i == -1:
            loader_i = random.choices(population=range(len(schedule_sizes)), weights=loader_stat, k=1)
            try:
                (x, label) = get_next(mode, loader_i[0])
            except StopIteration:
                availability[loader_i] = 0
                loader_i = -1
                if np.count_nonzero(availability) == 0:
                    return {'encoder': encoder,
                            'decoder': decoder,
                            'regressor': regressor}, \
                           sum(losses) / len(losses), \
                           torch.cat(tardy_accs, dim=0).mean(dim=0).item(), torch.cat(value_accs, dim=0).mean(
                        dim=0).item(), correct / total if total else 0

        x, label = x.to(device), label.to(device)
        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()
        h = encoder.init_hidden(x.size(0))
        encoder_outputs, h = encoder(x, h)
        # First "previous decoder output" will be the -1 token
        decoder_input = torch.empty((1, 1, x.size(0)), device=device).fill_(0.5)
        # First decoder hidden state will be last encoder hidden state
        decoder_hidden = h
        output = []
        # amount of tardy jobs
        pred_value = regressor(encoder_outputs.sum(dim=1))
        for ii in range(label.size(2)):
            decoder_output, decoder_hidden, attn_weights = decoder(decoder_input.view(-1, 1, 1), decoder_hidden,
                                                                   encoder_outputs)
            decoder_input = decoder_output.unsqueeze(0).to(device)
            output.append(decoder_output)

        tardy_loss, argmin = criterion(torch.stack(output, dim=1), label.permute(0, 2, 1).unsqueeze(-1))

        # cause in all optimal solutions same number of tardy jobs
        opt_val = label[:, 0, :].squeeze(1).sum(dim=1).unsqueeze(1)
        value_acc = pred_value.round().eq(opt_val.round()).double().detach()
        value_accs.append(value_acc)

        tardy_pred = torch.zeros((label.size(0), label.size(2)))
        # prediction based on value - picking y2 highest probablities
        for i in range(len(pred_value)):
            val = min(int(pred_value[i].round().item()), label.size(2) - 1)
            if val != 0:
                output_i = torch.topk(torch.stack(output, dim=1).view(x.size(0), -1)[i].squeeze().detach(), val)
                tardy_pred[i, output_i[1]] = 1
        dest_solution = torch.gather(label, 1, argmin.view(-1, 1).unsqueeze(2).repeat(1, 1, label.size(2)))
        tardy_acc = tardy_pred.eq(dest_solution.squeeze().cpu()).all(dim=1).double().detach()
        tardy_accs.append(tardy_acc)

        loss = criterion2(pred_value, opt_val) + tardy_loss
        losses.append(loss.item())

        # evaluation list scheduling
        if mode == ModelMode.test:
            jobs = prepare_for_eval(x, torch.stack(output, dim=1))
            for i in range(x.size()[0]):
                ordered = early_tardy_sort_scheduling(jobs[i].tolist(), x.size()[1] - int(pred_value[i].round().item()))
                ordered = np.stack(ordered)
                ordered = ordered.astype(int)
                value = eval(ordered[:, 0], ordered[:, 1], ordered[:, 2])
                total += 1
                correct += 1 if opt_val[i].round().item() == value else 0

        if mode == ModelMode.train:
            loss.backward()
            encoder_optimizer.step()
            decoder_optimizer.step()
            regressor_optimizer.step()


best_tardy_acc = 0.
for epoch in range(epochs):

    encoder.train()
    decoder.train()
    regressor.train()

    _, loss, tardy_acc, val_acc, _ = run_epoch(mode=ModelMode.train)
    logger.info(f'Train(epoch={epoch}, loss={loss:.4f}, vA={val_acc:.4f}, tA={tardy_acc:.4f}).')

    encoder.eval()
    decoder.eval()
    regressor.eval()
    model, loss, tardy_acc, val_acc, _ = run_epoch(mode=ModelMode.valid)
    logger.info(f'Validation(epoch={epoch}, loss={loss:.4f}, vA={val_acc:.4f}, tA={tardy_acc:.4f}).')

    if best_tardy_acc < tardy_acc:
        logger.info('New best model found!')
        best_tardy_acc = tardy_acc
        torch.save(model['encoder'], 'encoder.pt')
        torch.save(model['decoder'], 'decoder.pt')
        torch.save(model['regressor'], 'regressor.pt')

encoder = torch.load('encoder.pt')
decoder = torch.load('decoder.pt')
regressor = torch.load('regressor.pt')
encoder.eval()
decoder.eval()
regressor.eval()
_, loss, tardy_acc, val_acc, sched_acc = run_epoch(mode=ModelMode.test)
logger.info(f'Test(epoch={0}, loss={loss:.4f}, vA={val_acc:.4f}, tA={tardy_acc:.4f}, sA={sched_acc:.4f}).')
