import numpy as np


def list_scheduling_alg(jobs, e):

    copy = np.copy(jobs)

    E = []
    for i in range(e):
        max_i = np.argmax(copy[:, 3], axis=0)
        E.append(max_i)
        copy[max_i, 3] = -1

    T = []
    for i in range(copy.shape[0]):
        if copy[i, 3] >= 0:
            T.append(i)

    scheduled = []
    t = 0
    while len(scheduled) < len(jobs):
        r_before_t, rbt_idx = [], []
        backlog = E if len(E) > 0 else T
        t = max(t, min(jobs[i][0] for i in backlog))
        for i in backlog:
            j = jobs[i]
            if j[0] <= t:
                r_before_t.append(j)
                rbt_idx.append(i)
        r_before_t = np.stack(r_before_t, axis=0)
        max_i = np.argmax(r_before_t[:, 3], axis=0)
        scheduled.append(r_before_t[max_i])
        #index in jobs
        abs_max_i = rbt_idx[max_i]
        if abs_max_i in E:
            E.remove(abs_max_i)
        else:
            T.remove(abs_max_i)
        t += jobs[abs_max_i][1]

    return scheduled


def early_tardy_sort_scheduling(jobs, e):
    # TODO: Study the misclassified examples: what can be improved in the heuristic?

    copy = np.copy(jobs)

    E = []
    for i in range(e):
        max_i = np.argmax(copy[:, 3], axis=0)
        E.append(max_i)
        copy[max_i, 3] = -1

    T = []
    for i in range(copy.shape[0]):
        if copy[i, 3] >= 0:
            T.append(i)

    scheduled = []
    done = set()
    t = 0
    while len(done) < len(E):
        # Try both options. max(t, jobs[e][0]) appears to give worse result.
        # Er = sorted(set(E) - done, key=lambda e: max(t, jobs[e][0]) + jobs[e][1])
        Er = sorted(set(E) - done, key=lambda e: jobs[e][0] + jobs[e][1])
        Ed = sorted(set(E) - done, key=lambda e: jobs[e][2] - jobs[e][1])

        ier = Er[0]
        ter = max(t, jobs[ier][0])
        ied = Ed[0]
        ted = max(t, jobs[ied][0])

        # sortedby r + p
        rpp = ter + jobs[ier][1]
        # sortedby d - p
        dmp = jobs[ied][2] - jobs[ied][1]
        if rpp <= dmp and ted > ter:
            done.add(ier)
            t = ter + jobs[ier][1]
            scheduled.append(jobs[ier])
        else:
            done.add(ied)
            t = ted + jobs[ied][1]
            scheduled.append(jobs[ied])

    # Append tardy jobs by EDF
    Td = sorted(T, key=lambda t: jobs[t][2])
    scheduled += [jobs[i] for i in Td]
    return scheduled


# r,p,d,probab
# jobs = np.array([[2, 2, 4, 0.4],
#                 [5, 4, 8, 0.2],
#                 [3, 3, 6, 0.3],
#                 [5, 5, 10, 0.7],
#                 [3, 4, 4, 0.3],
#                 [4, 8, 8, 0.9],
#                 [2, 2, 3, 0.5]])
# pprint(list_scheduling_alg(jobs, 1))
# print()
# pprint(list_scheduling_alg(jobs, 2))
# print()
# pprint(list_scheduling_alg(jobs, 3))
# print()
# pprint(list_scheduling_alg(jobs, 4))
# print()