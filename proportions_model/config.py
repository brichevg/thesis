import logging
import sys
from enum import IntEnum
from typing import NamedTuple, Any
from configparser import ConfigParser
from torch import device as TorchDevice
from torch.optim import Adam

from losses import MultipleSchedulingLoss


def get_logger(name, level=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


class ModelMode(IntEnum):
    train = 0
    eval = 1


class DbOpts(NamedTuple):
    db_name: str = 'evgeniya'
    db_user: str = 'postgres'
    db_password: str = ''
    db_host: str = 'localhost'
    db_port: int = 5432

    # db_name: str = "scheduling_data_2"
    # db_user: str = "suchap"
    # db_password: str = "NeuralBenCompa1122"
    # db_host: str = "mongol.ciirc.cvut.cz"
    # db_port: int = 5432


class Hyperparameters(NamedTuple):
    lr: float = 0.01 * 32./256
    batch_size: int = 64
    epochs: int = 1000


class SchedulingModelOpts(NamedTuple):
    # hidden_size: int = 2000
    hidden_size: int = 200
    input_size: int = 4
    output_size: int = 1
    teacher_forcing_prob: float = 0.5
    optimizer: Any = Adam
    loss: Any = MultipleSchedulingLoss


class Config:
    device = TorchDevice('cuda')
    db_opts = DbOpts()
    hyperparams = Hyperparameters()
    sch_m_opts = SchedulingModelOpts()


class ConfigFile(Config):
    def __init__(self):
        config = ConfigParser()
        config.read('config.ini')
        self.device = TorchDevice(config.get('GENERAL', 'device', fallback=None))
        self.db_opts = DbOpts(
            db_name=config.get('DB', 'db_name', fallback=None),
            db_user=config.get('DB', 'db_user', fallback=None),
            db_password=config.get('DB', 'db_password', fallback=None),
            db_host=config.get('DB', 'db_host', fallback=None),
            db_port=config.getint('DB', 'db_port', fallback=None)
        )
        self.hyperparams = Hyperparameters(
            lr=config.getfloat('HYPERPARAMETERS', 'lr', fallback=None),
            batch_size=config.getint('HYPERPARAMETERS', 'batch_size', fallback=None),
            epochs=config.getint('HYPERPARAMETERS', 'epochs', fallback=None)
        )

