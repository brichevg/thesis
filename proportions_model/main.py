import sys
import torch
import random
import numpy as np
from timeit import default_timer as timer

from torch import nn
import torch.optim as optim
import logging
from db_connector import DbConnector
from db_connector_2 import DbConnector2

from loader_1 import Loader
# from config import get_logger
from losses import SchedulingLoss
from net import Encoder, BahdanauDecoder
from config import Config, ModelMode, get_logger

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

print(device)
# connector = DbConnector(
#     db_name="evgeniya",
#     db_user="postgres",
#     db_password="",
#     db_host="localhost",
#     db_port="5432"
# )

connector = DbConnector(db_name=sys.argv[1],
                        db_user=sys.argv[2],
                        db_password=sys.argv[3],
                        db_host=sys.argv[4],
                        db_port="5432")
config = Config()

# LSTM params
hidden_size = config.sch_m_opts.hidden_size
input_size = config.sch_m_opts.input_size
output_size = config.sch_m_opts.output_size

# min_distance_model
encoder = Encoder(input_size, hidden_size, device).to(device)
decoder = BahdanauDecoder(output_size, hidden_size, output_size).to(device)

# model params
lr = config.hyperparams.lr
batch_size = config.hyperparams.batch_size
encoder_optimizer = optim.Adam(encoder.parameters(), lr=lr)
decoder_optimizer = optim.Adam(decoder.parameters(), lr=lr)
EPOCHS = config.hyperparams.epochs
teacher_forcing_prob = config.sch_m_opts.teacher_forcing_prob
# criterion = SchedulingLoss(nn.L1Loss())
criterion = SchedulingLoss(nn.MSELoss(reduction='none'))

old_val_loss = np.inf
# loader = Loader(connector, device, [5, 6, 7, 8, 9, 10, 15, 20, 25, 30])
# for epoch in range(1, EPOCHS + 1):
#     encoder.train()
#     decoder.train()
#     # print("here1")
#     offset = 0
#     avg_loss = 0
#
#     # per sample
#     accuracy_train = []
#     accuracy_valid = []
#     accuracy_test = []
#
#     loss_train = []
#     loss_validation = []
#     loss_test = []
#     # print("here2")
#     for i, (padded_x, mask, x_lengths, labels, _) in enumerate(loader.training):
#         # learn by a single problem
#         # print("problem "+str(i))
#         loss = 0
#         encoder_optimizer.zero_grad()
#         decoder_optimizer.zero_grad()
#         h = encoder.init_hidden(padded_x.size(0))
#         encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)
#
#         # First "previous decoder output" will be the -1 token
#         decoder_input = torch.empty((padded_x.size(0), 1, 1), device=device).fill_(-1)
#         # First decoder hidden state will be last encoder hidden state
#         decoder_hidden = h
#         output = []
#
#         # some of the sentences (schedules) are with the teacher forcing
#         teacher_forcing = True if random.random() < teacher_forcing_prob else False
#
#         # sum of differences between label and job
#         accuracy_sum = 0
#         accuracy_by_instance_all = 0
#         accuracy_instace = 1
#         output = []
#         for ii in range(labels.size(2)):
#             decoder_output, decoder_hidden, attn_weights, _ = decoder(decoder_input, decoder_hidden, encoder_outputs,
#                                                                       mask[:, :, 0].unsqueeze(2))
#             # 1 - tardy, 0 - not tardy
#             # predicted_value = 1. if decoder_output > 0.5 else 0.
#             if teacher_forcing:
#                 decoder_input = labels[:, :, ii].to(device)
#             else:
#                 decoder_input = decoder_output.to(device)
#
#             loss = loss + criterion(
#                 decoder_output.view(1, -1), labels[:, :, ii].view(1, -1),
#                 mask[:, ii, 0].view(1, -1), x_lengths.view(1, -1)
#             )
#
#             accuracy_sum += abs(
#                 decoder_output.view(1, -1) * mask[:, ii, 0].view(1, -1) - labels[:, :, ii].view(1, -1))
#             # print(f'Validation loss is {loss}.')
#         if epoch > 1:
#             loss.backward()
#             encoder_optimizer.step()
#             decoder_optimizer.step()
#         # avg_loss += loss.item() / len(train_items)
#         accuracy_train.append((accuracy_sum.detach() / x_lengths.view(1, -1)).sum() / padded_x.size(0))
#         loss_train.append(loss.detach().item())
#
#     with torch.no_grad():
#         # validation
#         encoder.eval()
#         decoder.eval()
#
#         offset = 0
#
#         for padded_x, mask, x_lengths, labels, _ in loader.validation:
#             # learn by a single problem
#             loss = 0
#             # print("problem valid " + str(i))
#             h = encoder.init_hidden(padded_x.size(0))
#             encoder_optimizer.zero_grad()
#             decoder_optimizer.zero_grad()
#             # jobs = torch.stack(jobs).unsqueeze(0).to(device)
#             encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)
#
#             # First "previous decoder output" will be the -1 token
#             decoder_input = torch.empty((padded_x.size(0), 1), device=device).fill_(-1)
#             # First decoder hidden state will be last encoder hidden state
#             decoder_hidden = h
#             output = []
#
#             # some of the sensences (schedules) are with the teacher forcing
#             accuracy_sum = 0
#             for ii in range(labels.size(2)):
#                 decoder_output, decoder_hidden, attn_weights, _ = \
#                     decoder(decoder_input, decoder_hidden, encoder_outputs, mask[:, :, 0].unsqueeze(2))
#                 decoder_input = decoder_output.to(device)
#                 loss = loss + criterion(
#                     decoder_output.view(1, -1), labels[:, :, ii].view(1, -1),
#                     mask[:, ii, 0].view(1, -1), x_lengths.view(1, -1)
#                 )
#                 accuracy_sum += abs(
#                     decoder_output.view(1, -1) * mask[:, ii, 0].view(1, -1) - labels[:, :, ii].view(1, -1))
#             accuracy_valid.append((accuracy_sum / x_lengths.view(1, -1)).sum() / padded_x.size(0))
#             loss_validation.append(loss.item())
#
#         if old_val_loss > sum(loss_validation) / len(loss_validation):
#             old_val_loss = sum(loss_validation) / len(loss_validation)
#             torch.save(encoder, 'encoder.pt')
#             torch.save(decoder, 'decoder.pt')
#         acc_train = 1. - (sum(accuracy_train) / len(accuracy_train)).item()
#         print(f'Train(epoch={epoch}, loss={sum(loss_train) / len(loss_train):.4f}, mA={(acc_train):.4f}).')
#         acc_validation = 1. - (sum(accuracy_valid) / len(accuracy_valid)).item()
#         print(
#             f'Validation(epoch={epoch}, loss={sum(loss_validation) / len(loss_validation):.4f}, mA={(acc_validation):.4f}).')
# # test
# encoder = torch.load("./model_lr0002_batch32_hiddensize_512/encoder.pt")
# decoder = torch.load("./model_lr0002_batch32_hiddensize_512/decoder.pt")
# encoder.eval()
# decoder.eval()
#
# offset = 0
# accuracy_test = []
# loss_test = []
# time_total = []
#
# with torch.no_grad():
#     for j, (padded_x, mask, x_lengths, labels, _) in enumerate(loader.test):
#         loss = 0
#         h = encoder.init_hidden(padded_x.size(0))
#         start = timer()
#         encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)
#         # First "previous decoder output" will be the -1 token
#         decoder_input = torch.empty((padded_x.size(0), 1), device=device).fill_(-1)
#         # First decoder hidden state will be last encoder hidden state
#         decoder_hidden = h
#         output = []
#         accuracy_sum = 0
#         for ii in range(labels.size(2)):
#             decoder_output, decoder_hidden, attn_weights, _ = decoder(decoder_input, decoder_hidden, encoder_outputs,
#                                                                       mask[:, :, 0].unsqueeze(2))
#             decoder_input = decoder_output.to(device)
#             loss = loss + criterion(
#                 decoder_output.view(1, -1), labels[:, :, ii].view(1, -1),
#                 mask[:, ii, 0].view(1, -1), x_lengths.view(1, -1)
#             )
#             accuracy_sum += abs(decoder_output.view(1, -1) * mask[:, ii, 0].view(1, -1) - labels[:, :, ii].view(1, -1))
#         accuracy_test.append((accuracy_sum / x_lengths.view(1, -1)).sum() / padded_x.size(0))
#         loss_test.append(loss.item())
#         end = timer()
#
#         total = end - start
#         time_total.append(total)
#
#     acc_test = 1. - (sum(accuracy_test) / len(accuracy_test)).item()
#     print(f'TEST( time = {total:.4f},MSE={sum(loss_test) / len(loss_test):.4f}, MAE={(acc_test):.4f}).')
#     # print(f'TEST( time = {1000*total:.4f},jobs amount={i:.4f}).')
encoder = torch.load("./model_lr0002_batch32_hiddensize_512/encoder.pt")
decoder = torch.load("./model_lr0002_batch32_hiddensize_512/decoder.pt")
encoder.eval()
decoder.eval()

offset = 0
accuracy_test = []
loss_test = []
time_total = []

for i in [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75]:
    loader = Loader(connector, device, [i])

    with torch.no_grad():
        for j, (padded_x, mask, x_lengths, labels, _) in enumerate(loader.test):
            loss = 0
            h = encoder.init_hidden(padded_x.size(0))
            start = timer()
            encoder_outputs, h = encoder(padded_x, x_lengths.view(-1), h)
            # First "previous decoder output" will be the -1 token
            decoder_input = torch.empty((padded_x.size(0), 1), device=device).fill_(-1)
            # First decoder hidden state will be last encoder hidden state
            decoder_hidden = h
            output = []
            accuracy_sum = 0
            for ii in range(labels.size(2)):
                decoder_output, decoder_hidden, attn_weights, _ = decoder(decoder_input, decoder_hidden, encoder_outputs, mask[:, :, 0].unsqueeze(2))
                decoder_input = decoder_output.to(device)
                loss = loss + criterion(
                    decoder_output.view(1, -1), labels[:, :, ii].view(1, -1),
                    mask[:, ii, 0].view(1, -1), x_lengths.view(1, -1)
                )
                accuracy_sum += abs(decoder_output.view(1, -1)*mask[:, ii, 0].view(1, -1) - labels[:, :, ii].view(1, -1))
            accuracy_test.append((accuracy_sum / x_lengths.view(1, -1)).sum() / padded_x.size(0))
            loss_test.append(loss.item())
            end = timer()

            total = end - start
            time_total.append(total)

        acc_test = 1. - (sum(accuracy_test) / len(accuracy_test)).item()
        print(f'TEST( time = {total:.4f},MSE={sum(loss_test) / len(loss_test):.4f}, MAE={(acc_test):.4f}).')
        # print(f'TEST( time = {1000*total:.4f},jobs amount={i:.4f}).')


