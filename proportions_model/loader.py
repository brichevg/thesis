from torch.utils.data import DataLoader
from JobsDataset import JobsDataset
from db_connector_2 import DbConnector2
import torch
import logging

from config import get_logger, Config
from padded_dataset import PaddedWrapperDataset, PaddedDatasetOptions


class Loader:
    def __init__(self, connector, device, schedule_size):
        self.logger = get_logger('loader')
        self.connector = connector
        self.config = Config
        padded_config_1 = PaddedDatasetOptions(max_size_x=(max(schedule_size), 3), max_size_y=(1, max(schedule_size)))
        self.training = PaddedWrapperDataset(padded_config_1)
        self.validation = PaddedWrapperDataset(padded_config_1)
        self.test = PaddedWrapperDataset(padded_config_1)
        self.load_all(device, schedule_size)
        # self.training = DataLoader(
        #     self.training, batch_size=64, shuffle=True, num_workers=0
        # )
        # self.validation = DataLoader(
        #     self.validation, batch_size=64, shuffle=False, num_workers=0
        # )
        self.test = DataLoader(
            self.test, batch_size=64, shuffle=False, num_workers=0
        )
        # self.logger.info(f'Loaded {len(self.training.dataset)} samples for training.')
        # self.logger.info(f'Loaded {len(self.validation.dataset)} samples for validation.')
        self.logger.info(f'Loaded {len(self.test.dataset)} samples for test.')

    def load(self, device, size):
        items = self.connector.read(size)
        offset = 0
        t_offset = round(len(items) / size * 0.60) * size
        v_offset = round(len(items) / size * 0.80) * size
        while offset < len(items) - size:
            # if offset < t_offset:
            #     where_to_store = self.training
            # elif offset < v_offset:
            #     where_to_store = self.validation
            # else:
            #     where_to_store = self.test
            where_to_store = self.test


            # vector of jobs in a schedule
            samples = []
            # vector of tardiness in a schedule
            labels = []
            for i in range(size):
                samples.append(
                    torch.Tensor(
                        [items[offset + i][1], items[offset + i][2], items[offset + i][3], float(items[offset + i][4])]).to(
                        device))
                labels.append(torch.Tensor([float(items[offset + i][4])]).to(device))

            where_to_store.push((torch.stack(samples), torch.stack(labels).view(1, -1)))
            offset += size

    def load_all(self, device, schedule_amounts):
        # TODO
        for i in schedule_amounts:
            self.logger.info(f'Loading schedules with {i} jobs.')
            self.load(device, i)
