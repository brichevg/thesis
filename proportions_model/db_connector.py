import psycopg2


class DbConnector:

    def __init__(self, db_name, db_user, db_password, db_host, db_port):
        connection = None
        try:
            print("creatin")
            connection = psycopg2.connect(
                database=db_name,
                user=db_user,
                password=db_password,
                host=db_host,
                port=db_port,
            )
            print("Connection to PostgreSQL DB successful")
        except psycopg2.OperationalError as e:
            print("The error occurred")
        self.connection = connection

    def create_connection(self, db_name, db_user, db_password, db_host, db_port):
        connection = None
        try:
            connection = psycopg2.connect(
                database=db_name,
                user=db_user,
                password=db_password,
                host=db_host,
                port=db_port,
            )
            print("Connection to PostgreSQL DB successful")
        except psycopg2.OperationalError as e:
            print("The error occurred")
        self.connection = connection

    def save_problem(self, r, p, d, t_matrix, best_val, jobs_amount, p_time):
        cursor = self.connection.cursor()

        # problem
        insert_problem = """ INSERT INTO scheduling_data.problems (best_val, opt_solution_amount, jobs_amount, p_time) VALUES (""" + str(
            str(best_val) + "," + str(len(t_matrix)) + "," + str(jobs_amount) +", "+str(p_time)) + """) RETURNING id"""
        cursor.execute(insert_problem, ())
        self.connection.commit()
        problem_id = cursor.fetchone()[0]
        labels = self.retrieve_labels(t_matrix)
        # jobs
        jobs = []
        for job_i in range(len(labels)):
            jobs.append((problem_id, r[job_i], p[job_i], d[job_i].item(), labels[job_i]))

        job_records = ", ".join(["%s"] * len(jobs))

        insert_jobs = (
            f"INSERT INTO scheduling_data.jobs (problem_id, r, p, d, tardy) VALUES {job_records}"
        )
        cursor.execute(insert_jobs, jobs)
        self.connection.commit()

    def retrieve_labels(self, t_matrix):
        labels = []
        counter = {}
        # vector of assignments for the problem
        for solution in t_matrix:
            for j in range(len(solution)):
                if j not in counter:
                    counter[j] = 0
                if solution[j]:
                    counter[j] += 1
        for j in range(len(counter)):
            labels.append(counter[j] / len(t_matrix))
        return labels

    def read(self, jobs_amount):
        print("KUKU")
        # select_problems = """select p.jobs_amount, j.r, j.p, j.d, j.tardy from problems p inner join jobs j on p.id = j.problem_id
        # where jobs_amount = """ + str(jobs_amount)

        select_problems = """select p.jobs_amount, j.r, j.p, j.d, j.tardy from scheduling_data.problems p inner join scheduling_data.jobs j on p.id = j.problem_id
                where jobs_amount = """ + str(jobs_amount) + """ and best_val in (1, 2, 3)"""

        # select_problems = """select p.jobs_amount, j.r, j.p, j.d, j.tardy, p.best_val, p.id from scheduling_data.problems p inner join scheduling_data.jobs j on p.id = j.problem_id where jobs_amount = """+str(jobs_amount)

        # select_problems = """select p.jobs_amount, j.r, j.p, j.d, j.tardy, p.best_val, p.id from scheduling_data.problems p inner join scheduling_data.jobs j on p.id = j.problem_id where best_val = """+str(jobs_amount)+""" and p.jobs_amount=6"""


        # select_problems = """select p.jobs_amount, j.r, j.p, j.d, j.tardy from problems p inner join jobs j
        #         where jobs_amount = """ + str(jobs_amount) + """ limit 10000"""
        cursor = self.connection.cursor()
        # result = None
        try:
            cursor.execute(select_problems)
            result = cursor.fetchall()
            return result
        except psycopg2.OperationalError as e:
            print("The error occurred")
