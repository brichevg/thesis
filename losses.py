import torch
import torch.nn as nn
from torch import Tensor

class SchedulingLoss(nn.Module):
    def __init__(self, criterion: nn.Module):
        super(SchedulingLoss, self).__init__()
        self.criterion = criterion

    def forward(self, output, labels, mask, lengths) -> Tensor:
        return (self.criterion(output * mask, labels * mask) / lengths).mean(dim=1)


class MinBCELoss(nn.Module):
    def __init__(self):
        super(MinBCELoss, self).__init__()
        self.criterion = nn.BCELoss(reduction='none')

    def forward(self, output, targets, mask_x, mask_y):
        """
        >> b = output = tensor([[[[0.7000],
            [0.7000]]],

            [[[0.9000],
            [0.7000]]]], dtype=torch.float64)
        >> a = targets = tensor([[[[1.],
            [1.]],

            [[0.],
            [1.]]],

            [[[1.],
            [1.]],

            [[0.],
            [1.]]]], dtype=torch.float64)

        >> mse(a,b.repeat_interleave(2, dim=1)).mean(dim=2).min(dim=1)
        torch.return_types.min(
            values=tensor([[0.0900],
                    [0.0500]], dtype=torch.float64),
            indices=tensor([[0],
                    [0]]))
        """
        # Expects y size N x 1 x T x 1
        # Expects target size N x S x T x 1
        S = targets.size(1)
        # Makes y size N x S x T x 1
        output = output.repeat_interleave(S, dim=1)
        mask_x = mask_x.repeat_interleave(S, dim=1)
        masked_output = output * mask_x
        masked_targets = targets * mask_x
        # Make sure that maximum loss exists in masked solutions so that they are not selected.
        masked_output[~mask_y.bool()] = 1
        masked_targets[~mask_y.bool()] = 0
        r, i = self.criterion(masked_output, masked_targets).mean(dim=2).min(dim=1)
        return r.mean(), i
        # return r.mean()


class CountSolutionLoss(nn.Module):
    def __init__(self):
        super(CountSolutionLoss, self).__init__()
        self.mse = nn.MSELoss()

    def forward(self, output, targets, mask_x, mask_y):
        batch_indexing = torch.arange(0, output.size(0)).long().view(output.size(0), 1)
        targets = targets[batch_indexing, 0, :, :]
        output *= mask_x
        targets *= mask_x
        return self.mse(output.sum(dim=2), targets.sum(dim=2))


class MultipleSchedulingLoss(nn.Module):
    def __init__(self, alpha=1., beta=2.):
        super(MultipleSchedulingLoss, self).__init__()
        self.alpha = alpha
        self.beta = beta
        self.min_bce = MinBCELoss()
        self.count_solution = CountSolutionLoss()

    def forward(self, output, targets, mask_x, mask_y):
        bce_loss, i = self.min_bce(output, targets, mask_x, mask_y)
        count_loss = self.count_solution(output, targets, mask_x, mask_y)
        return self.alpha * bce_loss + self.beta * count_loss, i