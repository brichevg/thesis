# inout args: db_user, db_password, db_host, schedule_size
import sys

import torch
import random
import numpy as np
from torch import nn
import torch.optim as optim
# from db_connector import DbConnector
from db_connector_2 import DbConnector2
from losses import CountSolutionLoss, MinBCELoss
import torch.nn.functional as F
from losses import SchedulingLoss

from loader_2 import Loader
from config import Config, ModelMode, get_logger
from net2 import Encoder, BahdanauDecoder, LuongDecoder, Attention

logger = get_logger('training_baseline')
config = Config()

if torch.cuda.is_available():
    # device = config.device
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

connector = DbConnector2(**config.db_opts._asdict())

# Setting up model
lr = config.hyperparams.lr
batch_size = config.hyperparams.batch_size
epochs = config.hyperparams.epochs
teacher_forcing_prob = config.sch_m_opts.teacher_forcing_prob

hidden_size = config.sch_m_opts.hidden_size
input_size = config.sch_m_opts.input_size
output_size = config.sch_m_opts.output_size
encoder = Encoder(input_size, hidden_size, device).to(device)

attention = Attention(hidden_size, method="concat")
decoder = LuongDecoder(hidden_size, output_size, attention=attention).to(device)
# decoder = BahdanauDecoder(output_size, hidden_size, output_size).to(device)
regressor = nn.Sequential(
    nn.Linear(hidden_size, 1),
    nn.ReLU(),
    # nn.Dropout(0.1),
    # nn.Linear(hidden_size // 2, 1)
).to(device)

encoder_optimizer = optim.Adam(encoder.parameters(), lr=lr)
decoder_optimizer = optim.Adam(decoder.parameters(), lr=lr)
regressor_optimizer = optim.Adam(regressor.parameters(), lr=lr)
# criterion = nn.BCELoss(reduction='none')
criterion = nn.BCELoss()
criterion2 = nn.L1Loss()
loader = Loader(connector, device, [6])


def run_epoch(loader, mode=ModelMode.train):
    mode_attr = {ModelMode.train: 'train', ModelMode.eval: 'eval'}.get(mode, 'train')
    encoder.__getattribute__(mode_attr)()
    decoder.__getattribute__(mode_attr)()
    offset = 0
    avg_loss = 0
    # tardy_count = {}
    instance_accuracy_train = []
    instance_accuracy_valid = []
    losses, tardy_accuracies, val_accuracies = [], [], []
    avg_acc, avg_bce_loss, avg_count_loss = [], torch.empty(0), torch.empty(0)
    for idx, (x, label) in enumerate(loader):
        x, label = \
            x.to(device), torch.abs(label).to(device)
        encoder_optimizer.zero_grad()
        decoder_optimizer.zero_grad()
        h = encoder.init_hidden(x.size(0))
        encoder_outputs, h = encoder(x, h)
        loss = 0
        accuracy_sum = 0
        # First "previous decoder output" will be the -1 token
        decoder_input = torch.empty((1, 1, x.size(0)), device=device).fill_(0.5)
        # First decoder hidden state will be last encoder hidden state
        decoder_hidden = h
        output = []
        tardy_loss = 0
        for ii in range(label.size(2)):
            # Luong:
            print('decoder input')
            print(decoder_input)
            print('decoder hidden')

            print(decoder_hidden)
            print('encoder outp')
            print(encoder_outputs)

            decoder_output, decoder_hidden, attn_weights = decoder(decoder_input.view(-1, 1, 1), decoder_hidden,
                                                                   encoder_outputs)
            teacher_forcing = True if random.random() < teacher_forcing_prob else False

            if teacher_forcing:
                decoder_input = label[:, :, ii].unsqueeze(0).to(device)
            else:
                decoder_input = decoder_output.unsqueeze(0).to(device)
            print("COMPARISON")
            print(decoder_output)
            print("-----")
            print(label[:, :, ii].unsqueeze(-1))
            print("END")
            tardy_loss = tardy_loss + criterion(decoder_output, label[:, :, ii].unsqueeze(-1))


            output.append(decoder_output)
        # BCE / BCE AND L1
        # y1 = torch.stack(output, dim=1).view(x.size(0), -1).round().detach()

        # tardy_acc = y1.eq(label.view(x.size(0), -1)).all(dim=1).double().detach()
        # tardy_accuracies.append(tardy_acc)

        # ONLY L1 (no regressor)
        # y2 = torch.stack(output, dim=1).view(x.size(0), -1)
        # val_opt = label.squeeze(1).sum(dim=1)
        # val_acc = y2.round().sum(dim=1).eq(val_opt.round()).double().detach()
        # val_accuracies.append(val_acc)
        # loss = criterion2(y2.sum(dim=1), val_opt) + tardy_loss

        # L1 w regressor
        y2 = regressor(encoder_outputs.sum(dim=1))
        val_opt = label.squeeze(1).sum(dim=1).unsqueeze(1)
        val_acc = y2.round().eq(val_opt.round()).double().detach()
        val_accuracies.append(val_acc)

        tardy_pred = torch.zeros((label.size(0), label.size(2)))
        # prediction based on value - picking y2 highest probablities
        for i in range(len(y2)):
            val = min(int(y2[i].round().item()), label.size(2) - 1)
            if val != 0:
                output_i = torch.topk(torch.stack(output, dim=1).view(x.size(0), -1)[i].squeeze().detach(),
                                      val)
                tardy_pred[i, output_i[1]] = 1
        tardy_acc = tardy_pred.eq(label.view(x.size(0), -1).cpu()).all(dim=1).double().detach()
        tardy_accuracies.append(tardy_acc)

        loss = criterion2(y2, val_opt) + tardy_loss
        losses.append(loss.item())

        if mode == ModelMode.train:
            loss.backward()
            encoder_optimizer.step()
            decoder_optimizer.step()
            regressor_optimizer.step()

        if idx % 1000 == -1:
            output = torch.stack(output)
            print("--------------", idx, "-------------")
            print("--------------")
            # print(output[:, 0].squeeze())
            print(output[:, 0].round().squeeze())
            print(label[0])
            print("--------------")
            # print(output[:, 10].squeeze())
            print(output[:, 10].round().squeeze())
            print(label[10])
            print("--------------")
            # print(output[:, 20].squeeze())
            print(output[:, 20].round().squeeze())
            print(label[20])
            print("--------------")
            # print(output[:, 30].squeeze())
            print(output[:, 30].round().squeeze())
            print(label[30])
            print("--------------END-------------")
    return sum(losses) / len(losses), torch.cat(tardy_accuracies, dim=0).mean(dim=0).item(), \
           torch.cat(val_accuracies, dim=0).mean(dim=0).item(), 0, 0


for epoch in range(epochs):
    loss, tardy_acc, val_acc, _, _ = run_epoch(loader=loader.training, mode=ModelMode.train)
    logger.info(f'Train(epoch={epoch}, loss={loss:.4f}, vA={val_acc:.4f}, tA={tardy_acc:.4f}).')
    loss, tardy_acc, val_acc, _, _ = run_epoch(loader=loader.validation, mode=ModelMode.eval)
    logger.info(f'Validation(epoch={epoch}, loss={loss:.4f}, vA={val_acc:.4f}, tA={tardy_acc:.4f}).')
    # loss, acc, bce, count = run_epoch(loader=loader.test, mode=ModelMode.eval)
    # logger.info(f'Test(epoch={epoch}, loss={loss:.4f}, mA={acc:.4f}, bce={bce:.4f}, count={count:.4f}).')
