#!/bin/bash
#SBATCH --job-name=train_seq2seq_model
#SBATCH --output=train_seq2seq_model.out
#SBATCH --time=08:00:00
#SBATCH --partition=gpu
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --mem=32GB

ml purge
ml load Anaconda3/5.0.1

. /opt/apps/software/Anaconda3/5.0.1/etc/profile.d/conda.sh
source activate nitchky_success

nvidia-smi

eval $1

echo finish
